# The PHONY directives specify that these targets are not files to avoid errors
.PHONY: server gui client clean

ifeq ($(OS),Windows_NT)
    GRADLE=.\gradlew
else
    GRADLE=./gradlew
endif


# Target all builds the project.
build:
	$(GRADLE) build

# Target run executes the program and start with target all to build the
# project.

	
server : 
	$(GRADLE) run --console=plain --args="--server"
	

client : 
	$(GRADLE) run --console=plain --args="--client"
	
gui :
	$(GRADLE) run --console=plain --args="--gui"

#guiWithClient :
#	$(GRADLE) run --console=plain --args="--guiWithClient"
	


# Target clean removes all files produced during build.
clean :
	$(GRADLE) clean
