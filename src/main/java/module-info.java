module treasurehunt.main {

    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.base;
    requires javafx.graphicsEmpty;
    requires javafx.baseEmpty;
    requires javafx.fxml;

    opens client to javafx.fxml;
    opens server to javafx.fxml;
    opens commun to javafx.fxml;

    exports client;
    exports server;
    exports commun;
    exports client.view;
    exports client.view.multiscene.controller;
    opens client.view to javafx.fxml, javafx.graphics;
    opens client.view.multiscene.controller to javafx.fxml, javafx.graphics;

}