package launcher;

import client.Client;
import client.view.ClientGUI;

import server.Server;

import java.util.Locale;

/**
 * Launcher class : the game is launched from here
 */
public class Launcher {
    public static void main(String[] args) {
        if (args.length == 0)
            System.out.println("Enter arguments");
        else if (args[0].equals("--gui")) {
            ClientGUI gui = new ClientGUI();
            gui.startGUI();
        } else if (args[0].equals("--server"))
            Server.main(new String[0]);
        else if (args[0].equals("--client"))
            Client.initThreadForTUI();
        // System.out.println();
        else {
            System.out.println("Invalid arguments.");
            return;
        }

    }
}
