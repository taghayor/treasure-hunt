package assets;

import javafx.scene.image.Image;


import java.io.*;

public class Images {
    private static Image ICON_IMAGE;
    private static Image HOLE_IMAGE, TREASURE_UNKNOWN_IMAGE, TREASURE_5_IMAGE, TREASURE_10_IMAGE, TREASURE_15_IMAGE, TREASURE_20_IMAGE, WALL_IMAGE, FLOOR_IMAGE, BARICADE_IMAGE, HIDDEN_BOX;
    private static Image MUSHROOM_IMAGE, TROLL_IMAGE, BRUTE_IMAGE, FAIRY_IMAGE, ELF_IMAGE, WIZARD_IMAGE, GUARDIAN_IMAGE, RANGER_IMAGE, ENT_IMAGE, GOLEM_IMAGE, KNIGHT_IMAGE, CLERIC_IMAGE;


    public static Image getFloorImage(int targetSize, boolean preserveRatio, boolean smooth) {
        if (FLOOR_IMAGE == null || !(FLOOR_IMAGE.getHeight() == targetSize || FLOOR_IMAGE.getWidth() == targetSize)) {
            try {
                FLOOR_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/box/floor_tile_main.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return FLOOR_IMAGE;
    }

    public static Image getHiddenBox(int targetSize, boolean preserveRatio, boolean smooth) {
        if (HIDDEN_BOX == null || !(HIDDEN_BOX.getHeight() == targetSize || HIDDEN_BOX.getWidth() == targetSize)) {
            try {
                HIDDEN_BOX = new Image(new FileInputStream("src/main/resources/images/pixel/box/invisible-box.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return HIDDEN_BOX;
    }

    public static Image getHoleImage(int targetSize, boolean preserveRatio, boolean smooth) {
        if (HOLE_IMAGE == null || !(HOLE_IMAGE.getHeight() == targetSize || HOLE_IMAGE.getWidth() == targetSize)) {
            try {
                HOLE_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/box/hole.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return HOLE_IMAGE;
    }


    public static Image getBaricadeImage(int targetSize, boolean preserveRatio, boolean smooth) {
        if (BARICADE_IMAGE == null || !(BARICADE_IMAGE.getHeight() == targetSize || BARICADE_IMAGE.getWidth() == targetSize)) {
            try {
                BARICADE_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/box/baricade.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return BARICADE_IMAGE;
    }

    public static Image getTreasureImage(boolean unknownScore, int tresScore, int targetSize, boolean preserveRatio, boolean smooth) {

        if (unknownScore)
            return getTreasureUnknown(targetSize, preserveRatio, smooth);
        else switch (tresScore) {

            case 20:
                return getTreasure20(targetSize, preserveRatio, smooth);
            case 15:
                return getTreasure15(targetSize, preserveRatio, smooth);
            case 10:
                return getTreasure10(targetSize, preserveRatio, smooth);
            default:
                return getTreasure5(targetSize, preserveRatio, smooth);

        }
    }

    public static Image getTreasureUnknown(int targetSize, boolean preserveRatio, boolean smooth) {
        if (TREASURE_UNKNOWN_IMAGE == null || !(TREASURE_UNKNOWN_IMAGE.getHeight() == targetSize || TREASURE_UNKNOWN_IMAGE.getWidth() == targetSize)) {
            try {
                TREASURE_UNKNOWN_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/box/treasure_unknown.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return TREASURE_UNKNOWN_IMAGE;
    }

    public static Image getTreasure5(int targetSize, boolean preserveRatio, boolean smooth) {
        if (TREASURE_5_IMAGE == null || !(TREASURE_5_IMAGE.getHeight() == targetSize || TREASURE_5_IMAGE.getWidth() == targetSize)) {
            try {
                TREASURE_5_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/box/treasure5.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return TREASURE_5_IMAGE;
    }

    public static Image getTreasure10(int targetSize, boolean preserveRatio, boolean smooth) {
        if (TREASURE_10_IMAGE == null || !(TREASURE_10_IMAGE.getHeight() == targetSize || TREASURE_10_IMAGE.getWidth() == targetSize)) {
            try {
                TREASURE_10_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/box/treasure10.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return TREASURE_10_IMAGE;
    }

    public static Image getTreasure15(int targetSize, boolean preserveRatio, boolean smooth) {
        if (TREASURE_15_IMAGE == null || !(TREASURE_15_IMAGE.getHeight() == targetSize || TREASURE_15_IMAGE.getWidth() == targetSize)) {
            try {
                TREASURE_15_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/box/treasure15.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return TREASURE_15_IMAGE;
    }

    public static Image getTreasure20(int targetSize, boolean preserveRatio, boolean smooth) {
        if (TREASURE_20_IMAGE == null || !(TREASURE_20_IMAGE.getHeight() == targetSize || TREASURE_20_IMAGE.getWidth() == targetSize)) {
            try {
                TREASURE_20_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/box/treasure20.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return TREASURE_20_IMAGE;
    }


    public static Image getWallImage(int targetSize, boolean preserveRatio, boolean smooth) {
        if (WALL_IMAGE == null || !(WALL_IMAGE.getHeight() == targetSize || WALL_IMAGE.getWidth() == targetSize)) {
            try {
                WALL_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/box/wooden-wall.png"), targetSize, targetSize, preserveRatio, smooth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return WALL_IMAGE;
    }


    public static Image getHumanPlayerImage(String imageName, int targetSize, boolean preserveRatio,
                                            boolean smooth) {
        switch (imageName) {
            case "brute":
                if (BRUTE_IMAGE == null || !(BRUTE_IMAGE.getHeight() == targetSize || BRUTE_IMAGE.getWidth() == targetSize)) {
                    try {
                        BRUTE_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/GnollBrute.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return BRUTE_IMAGE;

            case "cleric":
                if (CLERIC_IMAGE == null || !(CLERIC_IMAGE.getHeight() == targetSize || CLERIC_IMAGE.getWidth() == targetSize)) {
                    try {
                        CLERIC_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/NormalCleric.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {

                        e.printStackTrace();
                    }
                }
                return CLERIC_IMAGE;

            case "elf":
                if (ELF_IMAGE == null || !(ELF_IMAGE.getHeight() == targetSize || ELF_IMAGE.getWidth() == targetSize)) {
                    try {
                        ELF_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/elf-male.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return ELF_IMAGE;

            case "ent":
                if (ENT_IMAGE == null || !(ENT_IMAGE.getHeight() == targetSize || ENT_IMAGE.getWidth() == targetSize)) {
                    try {
                        ENT_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/Ent.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return ENT_IMAGE;

            case "fairy":
                if (FAIRY_IMAGE == null || !(FAIRY_IMAGE.getHeight() == targetSize || FAIRY_IMAGE.getWidth() == targetSize)) {
                    try {
                        FAIRY_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/fairy.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return FAIRY_IMAGE;

            case "golem":
                if (GOLEM_IMAGE == null || !(GOLEM_IMAGE.getHeight() == targetSize || GOLEM_IMAGE.getWidth() == targetSize)) {
                    try {
                        GOLEM_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/golem.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return GOLEM_IMAGE;

            case "guardian":
                if (GUARDIAN_IMAGE == null || !(GUARDIAN_IMAGE.getHeight() == targetSize || GUARDIAN_IMAGE.getWidth() == targetSize)) {
                    try {
                        GUARDIAN_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/forest-gaurdian.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return GUARDIAN_IMAGE;

            case "knight":
                if (KNIGHT_IMAGE == null || !(KNIGHT_IMAGE.getHeight() == targetSize || KNIGHT_IMAGE.getWidth() == targetSize)) {
                    try {
                        KNIGHT_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/ElvenKnight.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return KNIGHT_IMAGE;

            case "mushroom":
                if (MUSHROOM_IMAGE == null || !(MUSHROOM_IMAGE.getHeight() == targetSize || MUSHROOM_IMAGE.getWidth() == targetSize)) {
                    try {
                        MUSHROOM_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/NormalMushroom.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return MUSHROOM_IMAGE;

            case "ranger":
                if (RANGER_IMAGE == null || !(RANGER_IMAGE.getHeight() == targetSize || RANGER_IMAGE.getWidth() == targetSize)) {
                    try {
                        RANGER_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/Ranger.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return RANGER_IMAGE;

            case "troll":
                if (TROLL_IMAGE == null || !(TROLL_IMAGE.getHeight() == targetSize || TROLL_IMAGE.getWidth() == targetSize)) {
                    try {
                        TROLL_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/Troll.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return TROLL_IMAGE;

            case "wizard":
                if (WIZARD_IMAGE == null || !(WIZARD_IMAGE.getHeight() == targetSize || WIZARD_IMAGE.getWidth() == targetSize)) {
                    try {
                        WIZARD_IMAGE = new Image(new FileInputStream("src/main/resources/images/pixel/characters/Wizard.png"), targetSize, targetSize, preserveRatio, smooth);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                return WIZARD_IMAGE;
        }
        return null;
    }


    public static Image getIconImage() {
        if (ICON_IMAGE == null) {
            try {
                ICON_IMAGE = new Image(new FileInputStream("src/main/resources/images/icon.png"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return ICON_IMAGE;
    }

}
