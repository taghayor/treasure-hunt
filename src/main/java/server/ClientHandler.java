package server;

import commun.*;
import commun.GameMode;
import commun.element.Wall;
import commun.element.FixedElement;
import commun.element.Hole;
import commun.RevealBoardPart;
import commun.RevealTraps;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.util.function.Consumer;

import static commun.NetCode.*;

public class ClientHandler extends Thread {// TODO: voir si on aura besoin d'un bool pour savoir si le joueur est déjà
    // en game.
    private Socket client;
    private Server server;

    private DataInputStream streamIn;
    private DataOutputStream streamOut;

    private final Hashtable<Integer, Consumer<String[]>> commands = new Hashtable<>();

    private PHuman player;
    private boolean inGame = false;

    private GameInstance game;

    public ClientHandler(Server serv, Socket clt) throws IOException {
        server = serv;
        client = clt;

        streamOut = new DataOutputStream(clt.getOutputStream());
        streamOut.flush();
        streamIn = new DataInputStream(clt.getInputStream());
        player = new PHuman("");

    }

    {
        commands.put(LOGIN, this::setUsername);
        commands.put(CREATE_MAP, this::createMap);/// TODO: voir ça après
        commands.put(GET_MAP_LIST, this::sendMapList);
        commands.put(JOIN_MAP, this::mapJoining);
        commands.put(CONFIRM_PLAYER_JOINED, this::playerAck);
        commands.put(REQUEST_START, this::requestStart);
        commands.put(REQUEST_START_GAME, this::requestStartResponse);
        commands.put(GET_HOLES, this::getHoles);
        commands.put(GET_WALLS, this::getWalls);
        commands.put(GET_TREASURES, this::getTreasures);
        commands.put(UPDATED_POSITION_RESPONSE, this::updatedPositionResponse);
        commands.put(GO_RIGHT, this::moveValidation);
        commands.put(GO_LEFT, this::moveValidation);
        commands.put(GO_UP, this::moveValidation);
        commands.put(GO_DOWN, this::moveValidation);
        commands.put(GAME_OVER, this::gameOver);
        commands.put(TURN_UPDATED, this::turnUpdated);
        commands.put(REVEAL_HOLE, this::revealHole);
        commands.put(REVEAL_MAP, this::revealMap);
        // commands.put(CREATE_MAP_ALEA, this::createMap);
    }

    @Override
    public void run() {
        try {
            // tant que le client est connecté
            // THIS LINE DOESN'T WORK DON'T USE IT!!!
            // while (streamIn.available() != 0) {
            while (true) {
                String msg = streamIn.readUTF();

                try {
                    String[] msgPart = msg.split(" ");
                    int command = Integer.valueOf(msgPart[0]);
                    if (!commands.containsKey(command)) {
                        log("sent unknown command id " + command);
                        sendErrorDesc(ERR_INVALID_CMD);// TODO: à revoir
                    } else {
                        log("command " + msg);
                        commands.get(command).accept(msgPart);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // TODO
            // sortir de la boucle si le client a déconecté
            // System.out.println("Client déconecté");
            // fermer le flux et la session socket
            // close()
        } catch (IOException e) {
            log("got offline");
            this.disconnect();
            // This should works but I can try it for the moment
        }
    }

    private void setUsername(String[] infos) {
        try {
            String fullName = infos[3];
            String[] data = fullName.split("/");
            String name = data[0];
            // String heroName = data.length == 1 ? "elf" : data[1];
            String lastname = player.getFullName();

            if (name.length() > 20) {
                sendErrorDesc(ERR_TOO_LONG_LOGIN);
            } else if (server.containsName(fullName)) {
                sendErrorDesc(ERR_ALREADY_USED);
            } else {
                player.setFullName(fullName);
                log("changed his pseudo from " + lastname + " to " + fullName);
                streamOut.writeUTF(LOGIN_ANS + " WELCOME " + fullName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createMap(String[] parts) {
        int gameMode = Integer.parseInt(parts[2]);
        int x = Integer.parseInt(parts[4]);
        int y = Integer.parseInt(parts[5]);
        int h = Integer.parseInt(parts[7]);
        int n = Integer.parseInt(parts[9]);
        if (gameMode < 1 || gameMode > 3) { // checking if gameMode is valid
            sendErrorDesc(ERR_INVALID_GAMEMODE);
        }

        // création de board
        Board board = new Board(x, y, 10, n, h, true);/// TODO: revoir ça
        System.out.println("freezes here !!!!!!!!!!");
        // if (!board.validateBoard()) {
        // sendErrorDesc(ERR_INVALID_MAP_CONSTANTS);
        // System.out.println("problem with board");
        // }

        System.out.println("board created");
        // create a game with requested mode
        GameInstance game = new GameInstance(board, GameMode.values()[gameMode - 1], this);

        // Join the created game
        this.inGame = true;
        this.game = game;
        this.game.addClient(this);

        // Add the game to the global system
        server.getGameSystem().addGame(game);

        // Send back the server id
        String id = game.getGameId();
        log("created map id " + id);
        try {
            streamOut.writeUTF(MAP_CREATED + " MAP CREATED " + id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // });
    }

    private void sendMapList(String[] parts) {// TODO: avoir une liste des games non lancées.
        ArrayList<GameInstance> listOfGames = server.getGameSystem().getGames();
        try {
            ArrayList<GameInstance> startedGame = new ArrayList<>();
            ArrayList<GameInstance> nonStartedGame = new ArrayList<>();
            for (GameInstance gameInstance : listOfGames) {
                if (gameInstance.isStarted())
                    startedGame.add(gameInstance);
                else
                    nonStartedGame.add(gameInstance);
            }
            int i = 1;
            if (this.game == null) {
                streamOut.writeUTF(GET_MAP_LIST_ANS + " NUMBER " + (listOfGames.size() - startedGame.size()));
                for (GameInstance gameInstance : nonStartedGame) {
                    if (!gameInstance.isStarted()) {
                        streamOut.writeUTF(GET_MAP_LIST_ANS + " MESS " + i + " ID " + gameInstance.getGameId() + " "
                                + (gameInstance.getGameMode() + 1) + " " + gameInstance.getBoard().getHeight() + " "
                                + gameInstance.getBoard().getWidth() + " " + gameInstance.getBoard().getNbHoles() + " "
                                + gameInstance.getBoard().getNbTreasures());
                        i++;
                    }
                }
            } else {
                streamOut.writeUTF(GET_MAP_LIST_ANS + " NUMBER " + (listOfGames.size() - nonStartedGame.size()));
                for (GameInstance gameInstance : startedGame) {
                    if (gameInstance.isStarted()) {
                        streamOut.writeUTF(GET_MAP_LIST_ANS + " MESS " + i + " ID " + gameInstance.getGameId() + " "
                                + (gameInstance.getGameMode() + 1) + " " + gameInstance.getBoard().getHeight() + " "
                                + gameInstance.getBoard().getWidth() + " " + gameInstance.getBoard().getNbHoles() + " "
                                + gameInstance.getBoard().getNbTreasures());
                        i++;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void mapJoining(String[] parts) {
        try {
            String gameId = parts[2];
            GameInstance gameInstance = server.getGameSystem().getGameFromId(gameId);

            if (gameInstance != null) {
                streamOut.writeUTF(MAP_JOINED + " MAP " + gameId + " JOINED");
                server.broadcastToGame(gameInstance, c -> {
                    DataOutputStream out = c.getOutputStream();
                    try {
                        out.writeUTF(PLAYER_JOINED_BROADCAST + " " + this.player.getFullName() + " JOINED");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                this.inGame = true;
                this.game = gameInstance;

                gameInstance.addClient(this);

                this.server.removeFromLobby(this);
            }
            // else {
            // streamOut.writeInt(ERR_MAP_DOESNT_EXIT);
            // }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void playerAck(String[] parts) {
        log(parts[1] + " joined the lounge");
    }

    private boolean mapLeaving() {
        if (this.game == null)
            return false;

        this.game.removeFromGame(this);
        this.server.broadcastToGame(this.game, c -> {
            DataOutputStream out = c.getOutputStream();
            try {
                out.writeUTF(PLAYER_DIED_BROADCAST + " " + this.player.getFullName() + " DIED");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        if (this.game.getCreator() == this && this.game.getNbPlayers() != 0 && this.game.getNbPlayers() != 1) {
            this.game.setCreator(this.game.getClients().get(0));
        } else {
            if (this.game.getNbPlayers() == 0) {
                this.server.getGameSystem().removeGame(this.game);
            } else if (this.game.getNbPlayers() == 1) {
                try {
                    this.game.getClients().get(0).streamOut
                            .writeUTF(GAME_END_BROADCAST + " " + this.game.getWinner() + " WINS");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        this.game = null;

        return true;
    }

    private void requestStart(String[] parts) {
        this.game.incrementNbConfirmedPlayers();
        this.game.setStartedGame(true);
        server.broadcastToGameWithoutMe(this.game, this, c -> {
            DataOutputStream out = c.getOutputStream();
            try {
                out.writeUTF(REQUEST_START_GAME + " START REQUESTED");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void startGameBroadcast() {
        // Generate random player position
        this.game.setPlayersInTheMap();
        String listOfGames[] = {String.valueOf(GET_MAP_LIST), "GETLIST"};
        server.broadcastToGame(this.game, c -> c.sendMapList(listOfGames));

        if (this.game.getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue()) {
            for (ClientHandler cHandler : this.game.getClients()) {
                // cHandler.sendPlayerPosition(cHandler.getPlayer());
                this.sendNecessariesForWarFrog(cHandler, cHandler.player, false);
            }
        } else {
            String holes[] = {String.valueOf(GET_HOLES), "GETHOLES"};
            this.getHoles(holes);
            String walls[] = {String.valueOf(GET_WALLS), "GETWALLS"};
            this.getWalls(walls);
            String treasures[] = {String.valueOf(GET_TREASURES), "GETTREASURES"};
            this.getTreasures(treasures);
            for (Player p : this.game.getPlayers()) {
                updatePositionBroadcast(p);
            }
        }

        server.broadcastToGame(this.game, c -> {
            DataOutputStream out = c.getOutputStream();
            try {
                out.writeUTF(GAME_STARTED_BROADCAST + " GAME STARTED");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        if (this.game.getGameMode() != 0) {
            server.broadcastToGame(this.game, c -> {
                DataOutputStream out = c.getOutputStream();
                try {
                    out.writeUTF(PLAYER_TURN + " " + this.game.getClientTurn().player.getFullName() + " TURN");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private void abortGameBroadcast() {
        // il noous reste le teste pour voir
        server.broadcastToGame(this.game, c -> {
            DataOutputStream out = c.getOutputStream();
            try {
                out.writeUTF(GAME_ABORTED_BROADCAST + " START ABORDED " + this.game.getNbRefusedPlayers());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        int index = 0;
        int cpt = 1;
        StringBuilder str = new StringBuilder(GAME_ABORTED_BROADCAST + " MESS " + (index + 1) + " PLAYER ");
        int length = this.game.getNbRefusedPlayers() / 5 + this.game.getNbRefusedPlayers() % 5;
        String requests[] = new String[length];
        for (String fullName : this.game.getPlayersRefusedToJoin()) {
            if (cpt == 6) {
                requests[index] = str.toString().trim();
                index++;
                cpt = 1;
                str = new StringBuilder(GAME_ABORTED_BROADCAST + " MESS " + (index + 1) + " PLAYER ");
            }
            str.append(fullName + " ");
            cpt++;
        }
        requests[index] = str.toString().trim();
        for (String string : requests) {
            server.broadcastToGame(this.game, c -> {
                DataOutputStream out = c.getOutputStream();
                try {
                    out.writeUTF(string);
                    c.resetData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        resetData();
    }

    private void requestStartResponse(String parts[]) {/// à revoire après pour la partie graphique.
        String response = parts[2];
        if (response.equals("YES")) {
            this.game.incrementNbConfirmedPlayers();
        } else {
            this.game.incrementNbRefusedPlayers();
            this.game.addPlayerToRefusedList(this.player.getFullName());
        }
        if (this.game.allPlayersConfirmed()) {
            if (this.game.getGameMode() != 0) {
                Random random = new Random();
                this.game.setClientTurn(random.nextInt(this.game.getNbPlayers()));
            }
            startGameBroadcast();

        } else if (this.game.aPlayerRefusedToJoin() && this.game.allPlayersRepond()) {// TODO: add test if no
            abortGameBroadcast();
        }
    }

    private void getHoles(String[] parts) {
        // Can't use this command if you're playing with warfog
        if (this.game.getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue())
            return;
        broadcastElementList(GET_HOLES_ANS, this.game.getBoard().getListOfHoles(), this.game.getBoard().getNbHoles());
    }

    private void getWalls(String[] parts) {
        // Can't use this command if you're playing with warfog
        if (this.game.getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue())
            return;
        broadcastElementList(GET_WALLS_ANS, this.game.getBoard().getListOfWalls(), this.game.getBoard().getNbWalls());
    }

    private void getTreasures(String[] parts) {
        // Can't use this command if you're playing with warfog
        if (this.game.getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue())
            return;
        broadcastElementList(GET_TREASURES_ANS, this.game.getBoard().getListOfTreasure(),
                this.game.getBoard().getNbTreasures());
    }

    private ArrayList<String> makeElementList(int command, ArrayList<? extends FixedElement> liste,
                                              int nbFixedElements) {
        if (nbFixedElements == 0) {
            return (new ArrayList<String>());
        }

        int index = 0;
        int cpt = 1;
        StringBuilder str = new StringBuilder(command + " MESS " + (index + 1) + " POS ");

        ArrayList<String> requests = new ArrayList<String>();
        for (FixedElement e : liste) {
            if (cpt == 6) {
                requests.add(str.toString().trim());
                index++;
                cpt = 1;
                str = new StringBuilder(command + " MESS " + (index + 1) + " POS ");
            }
            str.append(e.getCaract() + " ");
            cpt++;
        }
        requests.add(str.toString().trim());
        return requests;
    }

    private void sendElementList(int command, ArrayList<? extends FixedElement> liste, int nbFixedElements) {
        try {
            streamOut.writeUTF(command + " NUMBER " + nbFixedElements);
            for (String string : makeElementList(command, liste, nbFixedElements)) {
                streamOut.writeUTF(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void broadcastElementList(int command, ArrayList<? extends FixedElement> liste, int nbFixedElements) {
        server.broadcastToGame(this.game, c -> c.sendElementList(command, liste, nbFixedElements));
    }

    private void sendPlayerPosition(Player p) {
        try {
            streamOut.writeUTF(UPDATE_POSITION_BROADCAST + " " + p.getFullName() + " POS " + p.getL() + " " + p.getC());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updatePositionBroadcast(Player p) {
        server.broadcastToGame(this.game, c -> {
            DataOutputStream out = c.getOutputStream();
            try {
                out.writeUTF(UPDATE_POSITION_BROADCAST + " " + p.getFullName() + " POS " + p.getL() + " " + p.getC());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void updatedPositionResponse(String[] parts) {
        // TODO : do something here
    }

    private void moveValidation(String[] parts) {
        boolean dead = false;
        int index = 0;
        try {
            if (this.game.getClientTurn() == null || this.game.getClientTurn().equals(this)) {
                Direction direction;
                switch (parts[1]) {
                    case "GOLEFT":
                        direction = Direction.MLeft;
                        break;
                    case "GORIGHT":
                        direction = Direction.MRight;
                        break;
                    case "GOUP":
                        direction = Direction.MTop;
                        break;
                    default:
                        direction = Direction.MBottom;
                        break;
                }

                ArrayList<Integer> coords = this.player.requestChangePosition(direction);
                int score = this.player.getPlayerScore();
                int code = this.game.movePlayer(this.player, coords);
                switch (code) {
                    case MOVE_OK:
                        streamOut.writeUTF(MOVE_OK + " MOVE OK");
                        if (this.game.getGameMode() == 2) {
                            this.sendPlayerPosition(this.player);
                            this.sendNecessariesForWarFrog(this.game.getClientTurn(), this.game.getClientTurn().player,
                                    true);
                        } else {
                            this.updatePositionBroadcast(this.player);
                        }
                        break;
                    case MOVE_OK_TREASURE:
                        int treasureScore = this.player.getPlayerScore() - score;
                        streamOut.writeUTF(MOVE_OK_TREASURE + " MOVE OK TRES " + treasureScore);
                        if (this.game.isFinished(this.player)) {
                            this.server.broadcastToGame(this.game, c -> {
                                DataOutputStream out = c.getOutputStream();
                                try {
                                    out.writeUTF(GAME_END_BROADCAST + " " + this.game.getWinner() + " WINS");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                        } else {
                            server.broadcastToGame(this.game, c -> {
                                DataOutputStream out = c.getOutputStream();
                                try {
                                    out.writeUTF(TREASURE_OPENED_BROADCAST + " " + this.player.getFullName() + " POS "
                                            + this.player.getL() + " " + this.player.getC() + " TRES " + treasureScore);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                        }
                        break;
                    case MOVE_BLOCKED:
                        streamOut.writeUTF(MOVE_BLOCKED + " MOVE BLOCKED");
                        break;
                    case MOVE_DEAD:// TODO : add it in the lobby
                        streamOut.writeUTF(MOVE_DEAD + " MOVE HOLE DEAD");
                        index = this.game.getClients().indexOf(this);
                        this.game.removeFromGame(this);
                        server.broadcastToGame(this.game, c -> {
                            DataOutputStream out = c.getOutputStream();
                            try {
                                out.writeUTF(PLAYER_DIED_BROADCAST + " " + this.player.getFullName() + " DIED");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                        if (this.game.isFinished(this.player)) {
                            this.server.broadcastToGame(this.game, c -> {
                                DataOutputStream out = c.getOutputStream();
                                try {
                                    out.writeUTF(GAME_END_BROADCAST + " " + this.game.getWinner() + " WINS");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                        }
                        dead = true;
                        break;
                }
                if (!this.game.isFinished(this.player) && this.game.getGameMode() != 0) {
                    this.game.nextClientTurn(index);
                    server.broadcastToGame(this.game, c -> {
                        DataOutputStream out = c.getOutputStream();
                        try {
                            out.writeUTF(PLAYER_TURN + " " + this.game.getClientTurn().player.getFullName() + " TURN");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    if (this.game.getGameMode() == 2) {
                        for (ClientHandler client : this.game.getClients()) {
                            if (!client.getPlayer().equals(this.player)) {
                                this.sendNecessariesForWarFrog(client, client.player, false);
                            }
                        }
                    }
                }
                if (dead)
                    resetData();

            } else {
                streamOut.writeUTF(NOT_YOUR_TURN + " NOT YOUR TURN");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void turnUpdated(String[] parts) {
    }

    private void sendNecessariesForWarFrog(ClientHandler nextClient, Player nextPlayer, boolean decrease) {
        // Broadcast next player
        try {
            // Warfog zone
            Board b = this.game.getBoard();

            // Send walls in visible area
            ArrayList<Wall> visibleWalls = b.getListOfVisibleWalls(nextPlayer);
            ArrayList<Hole> visibleHoles = new ArrayList<Hole>();
            if (nextPlayer.hasRbpHelp()) {
                RevealBoardPart help = nextPlayer.getRbpHelp();
                for (Hole hole : b.getListOfHoles()) {
                    if (help.isShowable(hole.getFixedL(), hole.getFixedC()))
                        visibleHoles.add(hole);
                }
                for (Wall wall : b.getListOfWalls()) {
                    if (help.isShowable(wall.getFixedL(), wall.getFixedC()))
                        visibleWalls.add(wall);
                }
            }
            nextClient.getOutputStream().writeUTF(SENDING_WALL + " SENDING HOLES");
            nextClient.sendElementList(SENDING_WALL, visibleWalls, visibleWalls.size());

            // Send all treasures
            nextClient.getOutputStream().writeUTF(SENDING_TRES + " SENDING TRES");
            nextClient.sendElementList(SENDING_TRES, b.getListOfTreasure(), b.getNbTreasures());

            // Send holes
            // Holes can be revealed with reveal traps help or reveal board part help
            RevealTraps rtHelp = nextPlayer.getRtHelp();
            int nbHolesInFrontOfMe = 0;
            if (nextPlayer.hasRtHelp()) {
                for (Hole h : b.getListOfHoles()) {
                    int l = h.getFixedL();
                    int c = h.getFixedC();

                    if (rtHelp.isShowable(l, c)) {
                        visibleHoles.add(h);
                    }
                }

            } else {
                nbHolesInFrontOfMe = b.getNbOfVisibleHoles(nextPlayer);
            }
            nextClient.getOutputStream().writeUTF(SENDING_HOLES + " SENDING HOLES");
            if (!nextPlayer.hasRbpHelp() && !nextPlayer.hasRtHelp()) {
                nextClient.getOutputStream().writeUTF(SENDING_HOLES + " NUMBER " + nbHolesInFrontOfMe);
            } else {
                nextClient.sendElementList(SENDING_HOLES, visibleHoles, visibleHoles.size() + nbHolesInFrontOfMe);
            }

            // Send players
            nextClient.sendPlayerPosition(nextPlayer);
            for (ClientHandler c : this.game.getVisiblePlayers(nextPlayer)) {
                nextClient.sendPlayerPosition(c.player);
                c.sendPlayerPosition(nextPlayer);

            }

            // Decrement help turns
            if (nextPlayer.hasRbpHelp() && decrease)
                nextPlayer.getRbpHelp().decreaseRounds();
            if (nextPlayer.hasRtHelp() && decrease)
                nextPlayer.getRtHelp().decreaseRounds();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void revealHole(String[] parts) {
        try {

            if (this.game.getClientTurn().equals(this)) {

                if (this.player.getPlayerScore() < RevealTraps.PRICE) {
                    sendErrorDesc(NOT_ENOUGH_POINTS);
                    return;
                }

                this.player.pay(RevealTraps.PRICE);

                RevealTraps help = new RevealTraps(this.game.getBoard(), this.player);
                this.player.setRtHelp(help);

                streamOut.writeUTF(HOLE_PAYMENT_VALIDATED + " PAYMENT VALIDATED");
                this.sendNecessariesForWarFrog(this, this.player, false);
            } else {
                streamOut.writeUTF(NOT_YOUR_TURN + " NOT YOUR TURN");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void revealMap(String[] parts) {
        try {
            if (this.game.getClientTurn().equals(this)) {

                if (this.player.getPlayerScore() < RevealBoardPart.PRICE) {
                    sendErrorDesc(NOT_ENOUGH_POINTS);
                    return;
                }

                this.player.pay(RevealBoardPart.PRICE);
                RevealBoardPart help = new RevealBoardPart(this.game.getBoard(), this.player,
                        Integer.parseInt(parts[3]), Integer.parseInt(parts[4]));
                this.player.setRbpHelp(help);

                streamOut.writeUTF(MAP_PAYMENT_VALIDATED + " PAYMENT VALIDATED");
                this.sendNecessariesForWarFrog(this, this.player, false);
            } else {
                streamOut.writeUTF(NOT_YOUR_TURN + " NOT YOUR TURN");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void gameOver(String[] parts) {
        this.server.getGameSystem().removeGame(this.game);
        resetData();
    }

    public DataInputStream getInputStream() {
        return streamIn;
    }

    public DataOutputStream getOutputStream() {
        return streamOut;
    }

    public PHuman getPlayer() {
        return player;
    }

    private void sendErrorDesc(int cmd) {
        try {
            streamOut.writeUTF(cmd + " " + getCommandDescription(cmd));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void close() { /// TODO: à voir car une fois clientSocket fermé tout les flux liés à lui seront
        /// fermés.
        try {
            streamIn.close();
            streamOut.close();
            client.close();
        } catch (IOException e) {
            log("failed to close streams");
            e.printStackTrace();
        }
    }

    private void disconnect() {
        server.removeFromLobby(this);
        this.mapLeaving();
    }

    private void log(String prompt) {
        System.out.println("[client " + player.getFullName() + " " + LocalTime.now() + "] " + prompt);
    }

    public void resetData() {
        this.inGame = false;
        this.game = null;
        String fullname = player.getFullName();
        this.player = new PHuman(fullname);
    }

    @Override
    public boolean equals(Object obj) {
        return this.player.equals(((ClientHandler) obj).player);
    }
}
