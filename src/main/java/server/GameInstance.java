package server;

import commun.Board;
import commun.GameMode;
import commun.Player;
import commun.Game;

import java.util.ArrayList;

public class GameInstance extends Game {
    private ArrayList<ClientHandler> clients;
    private ArrayList<String> playersRefusedToJoin;
    private ClientHandler creator;

    private boolean startedGame = false;
    private int nbConfirmedPlayers;
    private int nbRefusedPlayers;

    private ClientHandler clientTurn;

    public GameInstance(Board b, GameMode m, ClientHandler creator) {
        super(b, m);

        this.clients = new ArrayList<ClientHandler>();
        this.playersRefusedToJoin = new ArrayList<>();
        this.creator = creator;

        this.nbConfirmedPlayers = 0;
        this.nbRefusedPlayers = 0;
    }

    public void addClient(ClientHandler c) {
        this.clients.add(c);
        this.players.add(c.getPlayer());
    }

    public void setPlayersInTheMap() {
        this.board.placePlayersRandomly(players);
    }

    public ArrayList<ClientHandler> getClients() {
        return clients;
    }

    public boolean isStarted() {
        return this.startedGame;
    }

    public void setCreator(ClientHandler creator) {
        this.creator = creator;
    }

    public ClientHandler getCreator() {
        return creator;
    }

    public boolean containsName(String fullName) {
        for (Player x : this.players)
            if (x.getFullName() != null)
                if (x.getFullName().equals(fullName))
                    return true;
        return false;
    }

    public String getBoardDisplayAsString(ClientHandler p) {
        return mode == GameMode.BROUILLARD_DE_GUERRE ? this.board.displayBrouillardDeGuerre(p.getPlayer())
                : this.board.displaySpeedingContest_TourParTour();
    }

    public boolean removeFromGame(ClientHandler h) {
        if (!this.clients.contains(h))
            return false;
        this.clients.remove(h);
        this.players.remove(h.getPlayer());
        this.board.setElement(h.getPlayer().getL(), h.getPlayer().getC(), null);
        return true;
    }

    public ArrayList<ClientHandler> getVisiblePlayers(Player p) {
        ArrayList<ClientHandler> visible = new ArrayList<ClientHandler>();
        for (ClientHandler w : this.clients) {
            if (!w.getPlayer().equals(p) && p.isInShownPart(w.getPlayer().getL(), w.getPlayer().getC())) {
                visible.add(w);
            }
        }
        return visible;
    }

    public void incrementNbConfirmedPlayers() {
        this.nbConfirmedPlayers++;
    }

    public void incrementNbRefusedPlayers() {
        this.nbRefusedPlayers++;
    }

    public boolean allPlayersConfirmed() {
        System.out.println("nbP : " + this.nbConfirmedPlayers + " / allP: " + this.players.size());

        return this.nbConfirmedPlayers == this.players.size();
    }

    public boolean aPlayerRefusedToJoin() {
        return this.nbRefusedPlayers != 0;

    }

    public boolean allPlayersRepond() {
        return this.nbConfirmedPlayers + this.nbRefusedPlayers == this.players.size();
    }

    public void addPlayerToRefusedList(String fullName) {
        this.playersRefusedToJoin.add(fullName);
    }

    public void nextClientTurn(int prec) {
        int index = this.clients.indexOf(clientTurn);
        if (index == -1) {
            index = prec == this.clients.size() ? 0 : prec;
            this.clientTurn = this.clients.get(index);
            return;
        }
        this.clientTurn = this.clients.get(index == this.clients.size() - 1 ? 0 : index + 1);
    }

    public int getNbRefusedPlayers() {
        return nbRefusedPlayers;
    }

    public ArrayList<String> getPlayersRefusedToJoin() {
        return playersRefusedToJoin;
    }

    public int getNbPlayers() {
        return this.clients.size();
    }

    public void setClientTurn(int playerIndex) {
        this.clientTurn = this.clients.get(playerIndex);
    }

    public ClientHandler getClientTurn() {
        return clientTurn;
    }

    public void setStartedGame(boolean startedGame) {
        this.startedGame = startedGame;
    }
}