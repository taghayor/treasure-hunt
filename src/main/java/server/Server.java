package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.function.Consumer;

public class Server {
    public static final int PORT = 5000;

    private ServerSocket server;
    private ArrayList<ClientHandler> lobby;
    private GameSystem gameSystem;

    public Server(int port) throws IOException {
        server = new ServerSocket(port);

        gameSystem = new GameSystem();
        lobby = new ArrayList<ClientHandler>();
    }

    public void start() throws IOException {
        log("server started");

        // Client accept loop
        Socket client;
        while (true) {
            client = server.accept();

            ClientHandler handler = new ClientHandler(this, client);
            handler.start();

            // Add player to lobby
            lobby.add(handler);
            log("client " + handler.getPlayer().getFullName() + " accepted");
        }
    }

    public boolean containsName(String fullName) {/// à verifier après pour la condition de null.
        if (containsNameInLobby(fullName)) {
            return true;
        }
        if (gameSystem.containsName(fullName)) {
            return true;
        }
        return false;
    }

    public boolean containsNameInLobby(String fullName) {
        /// à verifier après pour la condition de null.
        for (ClientHandler x : lobby)
            if (x.getPlayer().getFullName() != null)
                if (x.getPlayer().getFullName().equals(fullName))
                    return true;
        return false;
    }

    public void broadcastToLobby(Consumer<ClientHandler> action) { /// envoyer un message pour tout les joueurs du
        /// serveur en lobby.
        for (ClientHandler h : lobby) {
            action.accept(h);
        }
    }

    public void broadcastToGame(GameInstance instance, Consumer<ClientHandler> action) {/// envoyer un message pour tout
        /// les joueurs d'une partie.
        for (ClientHandler h : instance.getClients()) {
            action.accept(h);
        }
    }

    public void broadcastToGameWithoutMe(GameInstance instance, ClientHandler me, Consumer<ClientHandler> action) {/// envoyer
        /// un
        /// message
        /// pour
        /// tout
        /// les joueurs d'une partie.
        for (ClientHandler h : instance.getClients()) {
            if (!h.equals(me)) {
                action.accept(h);
            }
        }
    }

    public void broadcastToEveryone(Consumer<ClientHandler> action) {/// envoyer un message pour tout les joueurs du
        /// lobby et de toutes les parties.
        broadcastToLobby(action);
        for (GameInstance g : gameSystem.getGames()) {
            broadcastToGame(g, action);
        }
    }

    public static void main(String[] args) {
        try {
            Server serv = new Server(Server.PORT);
            serv.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public GameSystem getGameSystem() {
        return gameSystem;
    }

    public void log(String prompt) {
        System.out.println("[server " + LocalTime.now().toString() + "] " + prompt);
    }

    public void removeFromLobby(ClientHandler h) {
        lobby.remove(h);
    }

    public void removeFromGame(ClientHandler h) {
        gameSystem.removeFromGame(h);
    }
}