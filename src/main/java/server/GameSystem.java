package server;

import java.util.ArrayList;


public class GameSystem {
    // équivalent à la classe globale.
    private ArrayList<GameInstance> listOfGames;/// la liste des games lancées.

    public GameSystem() {
        listOfGames = new ArrayList<>();
    }

    public void addGame(GameInstance g) {
        listOfGames.add(g);
    }

    public void removeGame(GameInstance g) {
        listOfGames.remove(g);
    }

    public ArrayList<GameInstance> getGames() {
        return listOfGames;
    }

    public GameInstance getGameFromId(String gameId) {
        for (GameInstance gameInstance : listOfGames) {
            if (gameInstance.getGameId().equals(gameId))
                return gameInstance;
        }
        return null;
    }


    public ArrayList<GameInstance> getListOfGames() {
        return listOfGames;
    }

    public boolean containsName(String fullName) {
        for (GameInstance g : listOfGames)
            if (g.containsName(fullName))
                return true;
        return false;
    }


    public void removeFromGame(ClientHandler h) {
        for (GameInstance g : listOfGames)
            if (g.removeFromGame(h) == true)
                return;

    }
}
