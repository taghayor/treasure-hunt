package commun;

/**
 * NETCODE : a class that contains all static ints referring to commands <br>
 * [FR] netcode : une classes contenant tous les entiers dans les commandes
 */

public final class NetCode {
    // Even numbers for client and odd for server

    /* In lobby */
    public static final int LOGIN = 100; // C -> S
    public static final int LOGIN_ANS = 101;// S->C
    public static final int CREATE_MAP = 110; // C -> S
    public static final int MAP_CREATED = 111;// S->C
    public static final int GET_MAP_LIST = 120;// C -> S
    public static final int GET_MAP_LIST_ANS = 121;// S->C
    public static final int JOIN_MAP = 130;// C -> S
    public static final int MAP_JOINED = 131;// S->C
    public static final int PLAYER_JOINED_BROADCAST = 140;
    public static final int CONFIRM_PLAYER_JOINED = 141;
    public static final int REQUEST_START = 150;// C -> S
    public static final int REQUEST_START_GAME = 152; // C -> S
    public static final int GAME_STARTED_BROADCAST = 153;// S->C
    public static final int GAME_ABORTED_BROADCAST = 154;// S->C

    /* Moves in game */
    /// tout les moves avec le meme code 200 // C -> S
    public static final int GO_RIGHT = 200;
    public static final int GO_LEFT = 202;
    public static final int GO_UP = 204;
    public static final int GO_DOWN = 206;
    ///
    public static final int MOVE_OK = 201;// S->C
    public static final int MOVE_BLOCKED = 202;// S->C
    public static final int MOVE_OK_TREASURE = 203;// S->C
    public static final int MOVE_DEAD = 666;// S->C

    /* HELPS */
    public static final int REVEAL_HOLE = 300;// C -> S
    public static final int HOLE_PAYMENT_VALIDATED = 301;// S->C
    public static final int REVEAL_MAP = 310;// C -> S
    public static final int MAP_PAYMENT_VALIDATED = 311;// S->C
    public static final int SENDING_HOLES = 320;// S->C
    public static final int HOLE_RECEIVED = 321;// C -> S
    public static final int SENDING_WALL = 330;// S->C
    public static final int WALL_RECEIVED = 331;// C -> S
    public static final int SENDING_TRES = 340;// S->C
    public static final int TRES_RECEIVED = 341;// C -> S

    /* Updates in game */
    public static final int GET_HOLES = 400;// C -> S
    public static final int GET_HOLES_ANS = 401;// S->C
    public static final int GET_TREASURES = 410;// C -> S
    public static final int GET_TREASURES_ANS = 411;// S->C
    public static final int GET_WALLS = 420;// C -> S
    public static final int GET_WALLS_ANS = 421;// S->C
    public static final int PLAYER_TURN = 500;// S->C
    public static final int TURN_UPDATED = 501;// C -> S
    public static final int UPDATE_POSITION_BROADCAST = 510;// S->C
    public static final int TREASURE_OPENED_BROADCAST = 511;// S->C
    public static final int UPDATED_POSITION_RESPONSE = 512;// C -> S
    public static final int PLAYER_DIED_BROADCAST = 520;// S->C
    public static final int PLAYER_DIED_UPDATED = 521;// C -> S
    public static final int GAME_END_BROADCAST = 530;// S->C
    public static final int GAME_OVER = 600;// C -> S
    /* Errors */// S->C
    public static final int ERR_ALREADY_USED = 901;
    public static final int ERR_TOO_LONG_LOGIN = 990;
    public static final int ERR_INVALID_CMD = 999;
    public static final int NOT_YOUR_TURN = 902;
    public static final int NOT_ENOUGH_POINTS = 905;

    /*
     * COMMANDES EN PLUS
     */
    public static final int ERR_ALREADY_LOGGEDIN = 903;
    public static final int ERR_MAP_DOESNT_EXIT = 904;
    public static final int ERR_INVALID_GAMEMODE = 906;
    public static final int ERR_INVALID_MAP_CONSTANTS = 907;
    public static final int ERR_ALREADY_PLAYING = 908;
    public static final int CREATE_MAP_ALEA = 112;
    public static final int LEAVE_MAP = 132;
    public static final int MAP_LEAVED = 133;
    public static final int PLAYER_LEAVED_BROADCAST = 142;

    /************************************************************** */
    public static String getCommandDescription(int cmd) {
        switch (cmd) {
            case 901:
                return "ERROR_name_already_used";
            case 902:
                return "ERROR_already_logged_in";
            case 990:
                return "ERROR_login_name_too_long";
            case 905:
                return "ERROR_Not_Enough_Points";
            case 906:
                return "ERROR_gamemode_entered_isn't_valid";
            case 999:
                return "ERROR_invalid_command";
            case 908:
                return "ERROR_you're_already_playing";
            default:
                return "your_command_has_been_received";
        }
    }
}