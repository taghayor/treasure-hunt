package commun;

import static commun.NetCode.*;

public class CommandBuilder {
    public static String hello(String username) {
        return String.format("%d HELLO PLAYER %s", LOGIN, username);
    }

    public static String create(GameMode m, int x, int y, int h, int n) {
        return String.format("%d CREATE %d SIZE %d %d HOLE %d TRES %d", CREATE_MAP, m.getValue() + 1, x, y, h, n);
    }

    public static String getlist() {
        return String.format("%d GETLIST", GET_MAP_LIST);
    }

    public static String join(String gameId) {
        return String.format("%d JOIN %s", JOIN_MAP, gameId);
    }

    public static String ack(String fullName) {
        return String.format("%d %s ACK", CONFIRM_PLAYER_JOINED, fullName);
    }

    public static String requestStart() {
        return String.format("%d REQUEST START", REQUEST_START);
    }

    public static String start(String r) {
        return String.format("%d START %s", REQUEST_START_GAME, r);

    }

    public static String getHoles() {
        return String.format("%d GETHOLES", GET_HOLES);
    }

    public static String getWalls() {
        return String.format("%d GETWALLS", GET_WALLS);
    }

    public static String getTreasures() {
        return String.format("%d GETTREASURES", GET_TREASURES);
    }

    public static String goRight() {
        return String.format("%d GORIGHT", GO_RIGHT);
    }

    public static String goLeft() {
        return String.format("%d GOLEFT", GO_RIGHT);
    }

    public static String goUp() {
        return String.format("%d GOUP", GO_RIGHT);
    }

    public static String goDown() {
        return String.format("%d GODOWN", GO_RIGHT);
    }

    public static String moveUpdated(String username) {
        return String.format("%d %s UPDATED", UPDATED_POSITION_RESPONSE, username);
    }

    public static String deahtUpdated(String username) {
        return String.format("%d %s UPDATED", PLAYER_DIED_UPDATED, username);
    }

    public static String gameOver() {
        return String.format("%d GAME OVER", GAME_OVER);
    }

    public static String turnUpdated() {
        return String.format("%d TURN UPDATED", TURN_UPDATED);
    }

    public static String revealHole() {
        return String.format("%d REVEAL HOLE", REVEAL_HOLE);
    }

    public static String revealMap(int x, int y) {
        return String.format("%d REVEAL MAP %d %d", REVEAL_MAP, x, y);
    }

    public static String holeReceived() {
        return String.format("%d HOLE RECEIVED", HOLE_RECEIVED);
    }

    public static String wallReceived() {
        return String.format("%d WALL RECEIVED", WALL_RECEIVED);
    }

    public static String tresReceived() {
        return String.format("%d TRES RECEIVED", TRES_RECEIVED);
    }
}