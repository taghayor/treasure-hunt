package commun;

import assets.Images;
import javafx.scene.image.Image;

/**
 * a class for human players <br>
 * [FR] classe pour les joueurs Humains
 */
public class PHuman extends Player {

    public PHuman(String fullName, int l, int c) {
        super(fullName, l, c);
    }

    public PHuman(String fullName) {
        super(fullName);
    }

    public PHuman() {
        super();
    }

    @Override
    public String toString() {
        return "H";
    }

    @Override
    public Image getImage(int targetSize, boolean preserveRatio, boolean smooth) {
        image = Images.getHumanPlayerImage(getHeroName(), targetSize, preserveRatio, smooth);
        return image;
    }

}
