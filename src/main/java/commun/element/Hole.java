package commun.element;

import assets.Images;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * a class that represents hole, une classe qui représente les trous sue le
 * plateau
 */
public class Hole extends FixedElement {

    public Hole(int l, int c) {
        super(l, c);
    }

    @Override
    public boolean isHole() {
        return true;
    }

    @Override
    public Image getImage(int targetSize, boolean preserveRatio, boolean smooth) {
        image = Images.getHoleImage(targetSize, preserveRatio, smooth);
        return image;
    }

    @Override
    public String toString() {
        return "O";
    }
}
