package commun.element;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * this is an abstract class that represents any element in the box it can be a
 * wall, a treasure or a hole
 */

public abstract class Element {

    protected Image image;

    public boolean isTreasure() {
        return false;
    }

    public boolean isWall() {
        return false;
    }

    public boolean isHole() {
        return false;
    }

    public boolean isPlayer() {
        return false;
    }

    public abstract Image getImage(int targetSize, boolean preserveRatio, boolean smooth);
}