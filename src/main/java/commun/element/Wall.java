package commun.element;

import assets.Images;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Wall extends FixedElement {
    public Wall(int l, int c) {
        super(l, c);
    }

    @Override
    public boolean isWall() {
        return true;
    }

    @Override
    public Image getImage(int targetSize, boolean preserveRatio, boolean smooth) {
        image = Images.getWallImage(targetSize, preserveRatio, smooth);
        return image;
    }

    @Override
    public String toString() {
        return "#";
    }
}
