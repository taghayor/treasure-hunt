package commun.element;

public enum ElementCode {
    EMPTY(0), WALL(1), HOLE(2), TREASURE(3), PLAYER(4);

    private int id;

    ElementCode(int id) {
        this.id = id;
    }

    public int getValue() {
        return this.id;
    }
}