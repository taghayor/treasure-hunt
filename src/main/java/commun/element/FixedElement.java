package commun.element;

/**
 * a subclass of element with fixed coordinates
 */
public abstract class FixedElement extends Element {
    private int fixedL, fixedC;

    FixedElement(int l, int c) {
        this.fixedL = l;
        this.fixedC = c;
    }

    public int getFixedC() {
        return fixedC;
    }

    public int getFixedL() {
        return fixedL;
    }

    public String getCaract() {
        return this.fixedL + " " + this.fixedC;
    }

    @Override
    public boolean equals(Object obj) {
        return ((FixedElement) obj).getCaract().equals(this.getCaract());
    }
}
