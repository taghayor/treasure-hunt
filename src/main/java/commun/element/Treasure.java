package commun.element;

import assets.Images;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Random;

public class Treasure extends FixedElement {
    private final int score;
    private boolean unknownScore;

    public Treasure(int score, int l, int c) {
        super(l, c);
        this.score = score;
        if (score == 0)
            unknownScore = true;
    }

    public Treasure(int l, int c) {
        super(l, c);
        int tresScoreSeed = new Random().nextInt(5);
        this.score = tresScoreSeed * 5;
        if (score == 0)
            unknownScore = true;
    }

    public int getScore() {
        if (score > 0)
            return score;
        else {
            int tresScoreSeed = new Random().nextInt(4) + 1;
            return tresScoreSeed * 5;
        }
    }

    @Override
    public boolean isTreasure() {
        return true;
    }

    @Override
    public Image getImage(int targetSize, boolean preserveRatio, boolean smooth) {
        image = Images.getTreasureImage(unknownScore, score, targetSize, preserveRatio, smooth);
        return image;
    }


    @Override
    public String toString() {
        return "$";
    }

    @Override
    public String getCaract() {
        return super.getCaract() + " " + score;
    }


}
