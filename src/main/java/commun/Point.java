package commun;

/**
 * calsse Point
 */
public class Point implements Comparable<Point> {
    private int l;
    private int c;

    public Point(int l, int c) {
        this.l = l;
        this.c = c;
    }

    public int getLine() {
        return l;
    }

    public int getColumn() {
        return c;
    }

    @Override
    public int compareTo(Point o) {
        if (this.l == o.l && this.c == o.c)
            return 0;
        else if (this.l > o.l && this.c > o.c)
            return 1;
        return -1;
    }
}
