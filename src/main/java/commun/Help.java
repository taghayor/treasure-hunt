package commun;

/**
 * a class for the help options in the fog of war mode <br>
 * [FR] aide dans le mode brouillard de guerre
 */
public abstract class Help {

    protected Board board;
    protected Player player;

    private int roundCount = 0;

    Help(Board b, Player p, int MAX_ROUNDS) {
        this.board = b;
        this.player = p;
        this.roundCount = MAX_ROUNDS;
    }

    public int getRoundCount() {
        return this.roundCount;
    }

    public boolean isConsumed() {
        return this.roundCount == 0;
    }

    public void decreaseRounds() {
        this.roundCount--;
    }

    public abstract boolean isShowable(int x, int y);
}
