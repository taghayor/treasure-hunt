package commun.test;

import commun.*;

import java.util.ArrayList;
import java.util.Scanner;

public class TestMovement {
    public static void main(String[] args) {
        System.out.println("afficahge du plateau");
        Game game = new Game(null, GameMode.TOUR_PAR_TOUR);
        for (int i = 0; i < 4; i++) {
            game.addPlayer(new PHuman());
        }
        game.getBoard().placePlayersRandomly(game.getPlayers());
        game.getBoard().displaySpeedingContest_TourParTour();
        System.out.println(game.getBoard().validateBoard());
        Scanner sc = new Scanner(System.in);
        while (true) {
            for (int i = 0; i < game.getPlayers().size(); i++) {
                System.out.printf("position and score of %d ieme player:  l = %d , c = %d , score = %d\n", i,
                        game.getPlayers().get(i).getL(), game.getPlayers().get(i).getC(),
                        game.getPlayers().get(i).getPlayerScore());
            }
            System.out.println("What player you want to move? (0-3)");
            int p = sc.nextInt();
            if (p >= 0 && p <= 3) {
                System.out.println("enter movement direction: (0-3");
                System.out.println(" MLeft = (0), MRight = (1), MTop = (2), MBottom = (3);");
                int d = sc.nextInt();
                if (d >= 0 && d <= 3) {
                    ArrayList<Integer> newCoordinates = game.getPlayers().get(p)
                            .requestChangePosition(Direction.values()[d]);
                    int l = newCoordinates.get(0);
                    int c = newCoordinates.get(1);
                    System.out.printf("l: %d c: %d\n", l, c);
                    game.movePlayer(game.getPlayers().get(p), newCoordinates);
                    game.getBoard().displaySpeedingContest_TourParTour();
                }
            }
        }
    }
}