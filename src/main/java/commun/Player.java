package commun;

import commun.element.*;

import java.util.ArrayList;
import java.util.UUID;

/**
 * class for Player extends Element, a player on the board is an element, it has
 * attributes like pseudo heroName,score ...
 */
public abstract class Player extends Element {
    // équivalent à la classe joueur.
    private String pseudo = "";
    private String heroName;
    private String fullName;
    private int score;
    private int l, c; // position
    private boolean inGame; // savoir si le joueur est déjà dans une partie existante.

    private RevealBoardPart rbpHelp = null;
    private RevealTraps rtHelp = null;

    Player(String fullName, int l, int c) {
        this(fullName);
        this.l = l;
        this.c = c;
    }

    Player(String fullName) {
        this.fullName = fullName;
        String[] data = fullName.split("/");
        // Have a specifc pseudo.
        this.pseudo = data[0];
        this.heroName = data.length == 1 ? "elf" : data[1];
    }

    Player() {
        // Generate a random psuedo.
        this.fullName = generateUniquePlayerId();
    }

    public String generateUniquePlayerId() {
        String id = UUID.randomUUID().toString();
        return id.substring(0, id.indexOf('-'));
    }

    public boolean isInGame() {
        return inGame;
    }

    public void firstPositionSet(int l, int c) {
        changePosition(l, c);
        this.inGame = true;
    }

    public void changePosition(int l, int c) {
        this.l = l;
        this.c = c;
    }

    public ArrayList<Integer> requestChangePosition(Direction d) {
        int newL = l, newC = c;
        switch (d.getDirId()) {
            case 0:// left
                newC = this.c - 1;
                break;
            case 1:// right
                newC = this.c + 1;
                break;
            case 2: // top
                newL = this.l - 1;
                break;
            case 3: // bottom
                newL = this.l + 1;
                break;
            default:
                break;
        }
        ArrayList<Integer> result = new ArrayList<>();
        result.add(newL);
        result.add(newC);
        return result;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public void pay(int price) {
        this.score -= price;
    }

    public int getPlayerScore() {
        return score;
    }

    public boolean hasRbpHelp() {
        if (this.rbpHelp == null)
            return false;
        if (this.rbpHelp.isConsumed())
            return false;
        return true;
    }

    public boolean hasRtHelp() {
        if (this.rtHelp == null)
            return false;
        if (this.rtHelp.isConsumed()) {
            return false;
        }
        return true;
    }

    public boolean isInShownPart(int l, int c) {
        int d = Math.abs(this.l - l) + Math.abs(this.c - c);

        if (this.hasRbpHelp() && this.hasRtHelp())
            return (d <= 2 || this.rbpHelp.isShowable(l, c) || this.rtHelp.isShowable(l, c));
        else if (this.hasRbpHelp()) {
            return (d <= 2 || this.rbpHelp.isShowable(l, c));
        } else if (this.hasRtHelp())
            return (d <= 2 || this.rtHelp.isShowable(l, c));
        return (d <= 2);
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    public String getHeroName() {
        return heroName;
    }

    @Override
    public boolean equals(Object obj) {
        Player p = (Player) obj;
        return this.fullName.equals(p.getFullName());
    }

    @Override
    public String toString() {
        return "P";
    }

    public int getL() {
        return l;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public void setL(int l) {
        this.l = l;
    }

    public String getPseudo() {
        return pseudo;
    }

    public RevealBoardPart getRbpHelp() {
        return rbpHelp;
    }

    public void setRbpHelp(RevealBoardPart rbpHelp) {
        this.rbpHelp = rbpHelp;
    }

    public RevealTraps getRtHelp() {
        return rtHelp;
    }

    public void setRtHelp(RevealTraps rtHelp) {
        this.rtHelp = rtHelp;
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {

        this.fullName = fullName;
        String[] data = fullName.split("/");
        // Have a specifc pseudo.
        this.pseudo = data[0];
        this.heroName = data.length == 1 ? "elf" : data[1];
    }
}
