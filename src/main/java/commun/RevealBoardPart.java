package commun;

/**
 * revealBoardPart c'est une classe aide utilisable dans le mode brouillard de
 * guerre
 */
public class RevealBoardPart extends Help {
    private int zoneX;
    private int zoneY;

    public static int PRICE = 10;

    public RevealBoardPart(Board b, Player p, int x, int y) {
        super(b, p, 3);

        this.zoneX = x;
        this.zoneY = y;
    }

    public int getZoneX() {
        return zoneX;
    }

    public int getZoneY() {
        return zoneY;
    }

    @Override
    public boolean isShowable(int x, int y) {
        return (this.zoneX <= x && this.zoneX + 2 >= x && this.zoneY <= y && this.zoneY + 2 >= y);
    }
}
