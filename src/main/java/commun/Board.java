package commun;

import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;

import commun.element.*;

/**
 * the board is the main calss where all the game occurs <br>
 * it is made of boxes[] with some attributes relative to the board like height
 * width ... <br>
 * 
 * [FR] cette calsse représente le plateau ou tout le jeu se tient dans un
 * tableau de cases (Box)
 * 
 * Note : pour faciliter l'affichage et la manipulation height a été modifiée en
 * rajoutant 2 cases , une au début et une autre à la fin
 */

public class Board {

    private Box[][] boxes;
    private int height;
    private int width;
    private int nbWalls;
    private int nbTreasures;
    private int nbHoles;
    private int size;
    private ArrayList<Wall> listOfWalls = new ArrayList<>();
    private ArrayList<Treasure> listOfTreasure = new ArrayList<>();
    private ArrayList<Hole> listOfHoles = new ArrayList<>();

    public Board(int height, int width, int nbWalls, int nbTreasures, int nbHoles, boolean autogen) {
        this.height = height;
        this.width = width;
        this.size = height * width; // no en a besoin pour simplifier certain calculs
        this.nbTreasures = nbTreasures;
        this.nbHoles = nbHoles;
        this.nbWalls = nbWalls;

        this.boxes = new Box[this.height + 2][this.width + 2];

        if (autogen) {

            generateRandomly();
            if (!validateBoard()) {
                makeBoardPlayable();
            }

        } else {

            for (int i = 1; i < this.height + 1; i++)
                for (int j = 1; j < this.width + 1; j++)
                    boxes[i][j] = new Box();
        }
    }

    public boolean isBorderBox(int l, int c) {
        if (l == 0 || l == height + 1)
            return true;
        if (c == 0 || c == width + 1)
            return true;
        return false;
    }

    public boolean isEmptyBox(int l, int c) {
        return boxes[l][c].getElement() == null;
    }

    public String displaySpeedingContest_TourParTour() {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < this.height + 2; i++) {
            for (int j = 0; j < this.width + 2; j++) {
                if (boxes[i][j] == null) // les boxes[][] à valeur null font la borne ils sont affichés en *
                    string.append("* ");
                else
                    string.append(boxes[i][j] + " ");
            }
            string.append("\n");
        }
        return string.toString();
    }

    public String displayBrouillardDeGuerre(Player p) { /// afficher le plateau pour un joueur donnée.
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < this.height + 2; i++) {
            for (int j = 0; j < this.width + 2; j++) {
                // We need to check manually as the element could be null
                boolean isTreasure = false;
                if (boxes[i][j] != null && boxes[i][j].getElement() != null) {
                    isTreasure = boxes[i][j].getElement().isTreasure();
                }

                if (boxes[i][j] == null) {
                    // les boxes[][] à valeur null font la borne ils sont affichés en *
                    string.append("* ");
                } else if (p.isInShownPart(i, j) || isTreasure) {
                    string.append(boxes[i][j] + " ");
                } else {
                    string.append("@ ");
                }
            }
            string.append("\n");
        }
        return string.toString();
    }

    /**
     * c'est la fonction qui génère un plateau avec des positions aléatoires selon
     * les params données au constructeur : nombre de trésors, nombre de trous et de
     * murs
     */
    public void generateRandomly() {

        listOfHoles.clear();
        listOfTreasure.clear();
        listOfWalls.clear();

        int countTreasures = this.nbTreasures;
        int countWalls = this.nbWalls;
        int countHoles = this.nbHoles;

        // génération aléatoire du plateau
        // 1ere étape création d'un plateau vide
        for (int i = 1; i < this.height + 1; i++) {
            for (int j = 1; j < this.width + 1; j++) {
                boxes[i][j] = new Box();
            }
        }

        int range = 10;

        Random r = new Random();
        int l, c;
        // mettre les murs
        while (countWalls > 0) {
            l = r.nextInt(height) + 1;
            c = r.nextInt(width) + 1;

            if (boxes[l][c].isEmpty()) {
                Wall w = new Wall(l, c);
                boxes[l][c].setElement(w);
                listOfWalls.add(w);
                countWalls--;
            }
        }

        // mettre les tresors

        while (countTreasures > 0) {
            l = r.nextInt(height) + 1;
            c = r.nextInt(width) + 1;

            if (boxes[l][c].isEmpty()) {
                Treasure t = new Treasure(l, c);
                boxes[l][c].setElement(t);
                listOfTreasure.add(t);
                countTreasures--;
            }
        }
        // mettre les trous

        while (countHoles > 0) {
            l = r.nextInt(height) + 1;
            c = r.nextInt(width) + 1;

            if (boxes[l][c].isEmpty()) {
                Hole h = new Hole(l, c);
                boxes[l][c].setElement(h);
                listOfHoles.add(h);
                countHoles--;
            }

        }

    }

    /**
     * une fonctions qui positionne les players aléatoirement sur le plateau
     * 
     * @param players
     */
    public void placePlayersRandomly(ArrayList<Player> players) {
        for (Player player : players) {
            while (true) {
                int l = (int) (Math.random() * height) + 1;
                int c = (int) (Math.random() * width) + 1;
                if (boxes[l][c].isEmpty()) {
                    boxes[l][c].setElement(player);
                    player.setC(c);
                    player.setL(l);
                    break;
                }
            }
        }
    }

    public boolean isBoxTreasure(int l, int c) {
        if (this.boxes[l][c].getElement() == null) {
            return false;
        }

        return this.boxes[l][c].getElement().isTreasure();
    }

    public boolean isBoxHole(int l, int c) {
        if (this.boxes[l][c].getElement() == null) {
            return false;
        }

        return this.boxes[l][c].getElement().isHole();
    }

    public boolean isBoxWall(int l, int c) {
        if (this.boxes[l][c].getElement() == null) {
            return false;
        }

        return this.boxes[l][c].getElement().isWall();
    }

    public boolean isBoxPlayer(int l, int c) {
        if (this.boxes[l][c].getElement() == null) {
            return false;
        }

        return this.boxes[l][c].getElement().isPlayer();
    }

    public int treasureScore(int l, int c) {
        return ((Treasure) this.boxes[l][c].getElement()).getScore();
    }

    public boolean validateMoveToBox(int l, int c) {
        return !isBorderBox(l, c)
                && (isEmptyBox(l, c) || !boxes[l][c].getElement().isWall() && !boxes[l][c].getElement().isPlayer());
    }

    /**
     * cette fonction test l'état de notre plateau s'il est jouable ou non
     * 
     * fonctionnement : on cherche un case vide dans le plateau puis on essaye
     * d'accéder à ces cases adjacentes et récursivement on essaye d'acceder à
     * toutes les cases du plateau sauf les murs et les trous (càd pas de cases
     * isolées derrière des murs et des trous) si on arrive à le faire alors le
     * plateau est jouable et on le valide
     * 
     * 
     * 
     * @return true si le plateau est valide (jouable), false sinon
     */
    public boolean validateBoard() {

        // on fait une copie du tableau
        Box[][] localboard = boxes.clone();
        boolean firstTest = false, secondTest;
        if (this.nbWalls <= ((this.size * 30) / 100)) {
            firstTest = true;
        }

        int nbPlayableBoxes = this.size - (this.nbWalls + this.nbHoles);

        int i = 1;
        int k = 1;
        boolean stop = false;
        // chercher la première case vide pour lancer la recursivité

        while (!stop && i <= this.height) {
            while (!stop && k <= this.width) {
                if (localboard[i][k].isEmpty()) {
                    stop = true;
                }
                k++;
            }
            i++;
        }

        int acc = 0;
        int checkNbOfPlayableBoxes = countPlayableBoxesRec(localboard, i, k, acc);
        System.out.println("number of playable boxes:\t " + checkNbOfPlayableBoxes);
        System.out.println("number of playable boxes(theorique) :\t" + nbPlayableBoxes);
        secondTest = nbPlayableBoxes == checkNbOfPlayableBoxes;

        System.out.println("first test :\t" + firstTest);
        System.out.println("second test :\t" + secondTest);

        return firstTest && secondTest;
    }

    /**
     * elle compte le nombre de cases jouables avec un fonctionnement récursif, elle
     * calcul le nombre de cases dans l'acc
     * 
     * @param localBoard
     * @param l
     * @param c
     * @param acc
     * @return
     */
    public int countPlayableBoxesRec(Box[][] localBoard, int l, int c, int acc) {

        if (l > this.height || l < 1 || c > this.width || c < 1)
            return acc;
        else if (localBoard[l][c].isVerified())
            return acc;
        else if (!localBoard[l][c].isEmpty()) {

            if (localBoard[l][c].getElement().isTreasure()) {
                acc++;
                localBoard[l][c].setVerified(true);
            }
            if (localBoard[l][c].getElement().isHole() || localBoard[l][c].getElement().isWall())
                return acc;

        } else { // pour les cases vides
            acc++;
            localBoard[l][c].setVerified(true);
        }

        acc = countPlayableBoxesRec(localBoard, l, c + 1, acc);
        acc = countPlayableBoxesRec(localBoard, l, c - 1, acc);
        acc = countPlayableBoxesRec(localBoard, l + 1, c, acc);
        acc = countPlayableBoxesRec(localBoard, l - 1, c, acc);

        return acc;
    }

    /**
     * une fonction qui réduit la valeur des params si on arrive pas à jouer
     */
    private void reduceParams() {

        Random r = new Random();
        int x = r.nextInt(2);
        if (x == 0)
            this.nbWalls--;
        else
            this.nbHoles--;
    }

    private void makeBoardPlayable() {

        for (int i = 0; i < 10; i++) {
            generateRandomly();
            if (validateBoard())
                return;
        }
        while (!validateBoard()) {
            reduceParams();
            generateRandomly();
        }
    }

    public void removeTreasure(Treasure t) {
        this.listOfTreasure.remove(t);
    }

    public void setElement(int x, int y, Element element) {
        if (this.boxes[x][y] != null)
            boxes[x][y].setElement(element);
    }

    public String pseudoAt(int x, int y) {
        return ((Player) (boxes[x][y].getElement())).getPseudo();
    }

    public Element getElement(int l, int c) {
        if (this.boxes[l][c] != null)
            return this.boxes[l][c].getElement();
        return null;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getNbHoles() {
        return nbHoles;
    }

    public int getNbWalls() {
        return nbWalls;
    }

    public int getNbTreasures() {
        return nbTreasures;
    }

    public ArrayList<Hole> getListOfHoles() {
        return listOfHoles;
    }

    public ArrayList<Treasure> getListOfTreasure() {
        return listOfTreasure;
    }

    public ArrayList<Wall> getListOfWalls() {
        return listOfWalls;
    }

    public ArrayList<Wall> getListOfVisibleWalls(Player p) {
        ArrayList<Wall> visible = new ArrayList<Wall>();
        for (Wall w : listOfWalls) {
            if (p.isInShownPart(w.getFixedL(), w.getFixedC())) {
                visible.add(w);
            }
        }
        return visible;
    }

    public int getNbOfVisibleHoles(Player p) {
        int cpt = 0;
        for (Hole w : listOfHoles) {
            if (p.isInShownPart(w.getFixedL(), w.getFixedC())) {
                cpt++;
            }
        }
        return cpt;
    }

    public int getSumTreaures() {
        int sum = 0;
        for (Treasure treasure : this.listOfTreasure) {
            sum += treasure.getScore();
        }
        return sum;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setNbHoles(int nbHoles) {
        this.nbHoles = nbHoles;
    }

    public void setNbTreasures(int nbTreasures) {
        this.nbTreasures = nbTreasures;
    }

    public Box[][] getBoxes() {
        return boxes;
    }

    public void setNbWalls(int nbWalls) {
        this.nbWalls = nbWalls;
    }

    public void setSize(int size) {
        this.size = size;
    }
}