package commun;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import commun.element.Treasure;

/**
 * this class represents a game
 * 
 * [FR] une partie du jeu
 * 
 */
public class Game {
    protected Board board;
    protected ArrayList<Player> players;
    protected GameMode mode;
    protected String id;

    /**
     * 
     * @param gameId an id number specific to a game
     */
    public Game(String gameId) {
        this.id = gameId;
        this.players = new ArrayList<>();
    }

    public Game(Board b, GameMode m) {
        // (height ,width ,nbwalls ,nbtreasures ,nbholes )
        this.board = Objects.requireNonNullElseGet(b, () -> new Board(10, 10, 10, 10, 5, true));
        this.players = new ArrayList<Player>();
        this.mode = m;
        this.id = generateUniqueGameId();
    }

    public void addPlayer(Player p) {
        if (!this.players.contains(p))
            this.players.add(p);
    }

    public void removePlayer(Player p) {
        if (p == null)
            return;
        board.setElement(p.getL(), p.getC(), null); // removing the player from the board
        this.players.remove(p);
    }

    private boolean movementValidation(int x, int y) {
        return this.board.validateMoveToBox(x, y);
    }

    public int movePlayer(Player p, ArrayList<Integer> newCoordinates) {
        int l = newCoordinates.get(0);
        int c = newCoordinates.get(1);
        int code = NetCode.MOVE_OK;
        if (!movementValidation(l, c)) { /// le changement de place n'est pas valide.
            return NetCode.MOVE_BLOCKED;
        }

        if (board.isBoxHole(l, c)) { /// isBoxHole contain the box validation
            return NetCode.MOVE_DEAD;
        }

        if (board.isBoxTreasure(l, c)) {
            changePlayerScore(p, board.treasureScore(l, c));
            this.board.removeTreasure((Treasure) this.board.getElement(l, c));
            code = NetCode.MOVE_OK_TREASURE;
        }

        board.setElement(l, c, p);
        board.setElement(p.getL(), p.getC(), null); // moving the player in the board
        p.changePosition(l, c);
        return code;
    }

    public boolean isFinished(Player p) {
        return this.board.getListOfTreasure().size() == 0 || this.players.size() == 1 || !canOthersWin(p);
    }

    public boolean canOthersWin(Player p) {
        int sumTreasures = this.board.getSumTreaures();
        for (Player player : this.players) {
            if (!player.equals(p) && player.getPlayerScore() + sumTreasures >= p.getPlayerScore())
                return true;
        }
        return false;
    }

    public String getWinner() {
        if (this.players.size() == 1)
            return this.players.get(0).getFullName();
        int score = 0;
        String fullName = "";
        for (Player player : this.players) {
            if (player.getPlayerScore() > score) {
                score = player.getPlayerScore();
                fullName = player.getFullName();
            }
        }
        return fullName;
    }

    public void changePlayerScore(Player p, int treasureScore) {
        p.addScore(treasureScore);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) || ((Game) obj).id.equals(this.id);
    }

    private String generateUniqueGameId() {
        return UUID.randomUUID().toString();
    }

    public String getGameId() {
        return this.id;
    }

    public int getGameMode() {
        return this.mode.getValue();
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public Player getPlayerByFullName(String fullName) {
        for (Player p : this.players) {
            if (p.getFullName().equals(fullName))
                return p;
        }
        return null;
    }

    public void setBoard(Board b) {
        board = b;
    }

    public Board getBoard() {
        return board;
    }

}
