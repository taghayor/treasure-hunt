package commun;

/**
 * enumération des direction
 */
public enum Direction {
    MLeft(0), MRight(1), MTop(2), MBottom(3);

    private int dirId;

    Direction(int dirId) {
        this.dirId = dirId;
    }

    public int getDirId() {
        return dirId;
    }
}
