package commun;

import commun.element.Element;

public class Box implements Cloneable {
    /// équivalent à la classe case.
    private boolean verified = false;
    private Element element;// le contenu d'une case

    public Box() {
        this.element = null;
    }

    public boolean isEmpty() {
        return (element == null);
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public Element getElement() {
        return this.element;
    }

    @Override
    public String toString() {
        if (this.isEmpty())
            return ".";
        else
            return element.toString();
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isVerified() {
        return verified;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
