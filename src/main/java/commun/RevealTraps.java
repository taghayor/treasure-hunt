package commun;

/**
 * revealTraps c'est une classe aide utilisable dans le mode brouillard de
 * guerre ça montre les trous
 */
public class RevealTraps extends Help {
    public static int PRICE = 5;

    public RevealTraps(Board b, Player p) {
        super(b, p, 5);
    }

    @Override
    public boolean isShowable(int x, int y) {
        int d = Math.abs(this.player.getL() - x) + Math.abs(this.player.getC() - y);
        return (d <= 2 && this.board.isBoxHole(x, y));
    }
}
