package commun;

/**
 * enum for game modes
 * 
 * [FR] les modes de jeu :
 * 
 * Speeding contest : le mode rapide en LIVE <br>
 * Tour par tour : à chaque fois vient le tour d'un joueur <br>
 * fog of war : brouillard de guerre : un mode ou on a une visibilité limité
 */
public enum GameMode {
    SPEEDING_CONTEST(0), TOUR_PAR_TOUR(1), BROUILLARD_DE_GUERRE(2);

    private int id;

    GameMode(int id) {
        this.id = id;
    }

    public int getValue() {
        return this.id;
    }
}