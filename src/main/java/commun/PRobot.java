package commun;

import javafx.scene.image.Image;

/**
 * une classes d'un joueur robot quand on souhaite implémenter un joueur robot
 * intelligent
 */
public class PRobot extends Player {
    PRobot(String pseudo) {
        super(pseudo);
    }

    @Override
    public Image getImage(int targetSize, boolean preserveRatio, boolean smooth) {
        // image = Images.getHoleImage(targetWidth, targetHeight, preserveRatio,
        // smooth);
        return image;
    }

}
