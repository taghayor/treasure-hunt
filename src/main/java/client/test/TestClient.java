package client.test;

import commun.NetCode;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.EOFException;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.function.Supplier;
import java.time.LocalTime;

import commun.PHuman;
import static commun.NetCode.*;

public class TestClient {
    private Socket socket;
    private static Scanner sc;

    private DataInputStream streamIn;
    private DataOutputStream streamOut;

    private int gameId;

    private final Hashtable<Integer, Supplier<Boolean>> commands = new Hashtable<>();

    private PHuman player;

    public TestClient(int port) throws IOException {
        socket = new Socket("localhost", port);
        streamIn = new DataInputStream(socket.getInputStream());
        streamOut = new DataOutputStream(socket.getOutputStream());
        player = new PHuman();
    }

    {
        commands.put(LOGIN, this::loginReq);
        commands.put(LOGIN_ANS, this::loginAns);
        commands.put(CREATE_MAP, this::createMapReq);
        commands.put(MAP_CREATED, this::createMapAns);
    }

    // TODO define a hashTable for managing responses like ClientHandler
    // TODO define methods like: acceptGameResponseID

    private void login(String fullName) {
        try {
            if (fullName.equals(""))
                fullName = player.generateUniquePlayerId();
            streamOut.writeInt(LOGIN);
            streamOut.writeUTF(fullName);
            player.setPseudo(fullName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean loginReq() {
        login(sc.nextLine());
        return true;
    }

    private boolean loginAns() {
        try {
            String fullName = streamIn.readUTF();
            if (!fullName.equals(player.getFullName())) {
                log("failed to login");
                login("");
            } else {
                log("logged in as " + fullName);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean createMapReq() {
        try {
            System.out.println("game mode?");
            int gameMode = sc.nextInt();
            System.out.println(" height and width?");
            int width = sc.nextInt(), height = sc.nextInt();
            System.out.println("nb of walls?");
            int nbOfWalls = sc.nextInt();
            System.out.println("nb of treasures?");
            int nbOfTrees = sc.nextInt();
            System.out.println("nb of holes?");
            int nbOfHoles = sc.nextInt();

            streamOut.writeInt(CREATE_MAP);
            streamOut.writeInt(gameMode);
            streamOut.writeInt(width);
            streamOut.writeInt(height);
            streamOut.writeInt(nbOfWalls);
            streamOut.writeInt(nbOfTrees);
            streamOut.writeInt(nbOfHoles);

            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean createMapAns() {
        try {
            int newId = streamIn.readInt();
            log("new map created with id " + Integer.toString(newId));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);

        try {
            // default name = test if main has no arguments
            TestClient client = new TestClient(5000);
            client.login(args.length == 0 ? client.getPlayer().getPseudo() : args[0]);

            Thread sender = new Thread(new Runnable() {
                int cmd;

                @Override
                public void run() {
                    while (true) {
                        try {
                            cmd = sc.nextInt();
                            client.applyCommand(cmd);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            sender.start();

            Thread receiver = new Thread(new Runnable() {
                int cmd;
                String msg;

                @Override
                public void run() {
                    // while (client.streamIn.available() != 0) {
                    while (true) {
                        try {
                            cmd = client.streamIn.readInt();
                            client.applyCommand(cmd);
                        } catch (EOFException e) {
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    client.close();
                    System.exit(0);
                }
            });
            receiver.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void applyCommand(int cmd) throws IOException {
        if (cmd >= 900) {
            log("error " + streamIn.readUTF());
        } else if (!commands.containsKey(cmd)) {
            log("we haven't implemented the method yet " + Integer.toString(cmd));
        } else {
            commands.get(cmd).get();
        }
    }

    public PHuman getPlayer() {
        return player;
    }

    public void log(String prompt) {
        System.out.println("[client " + LocalTime.now().toString() + "] " + prompt);
    }

    public void close() {
        log("disconnected");
        try {
            streamIn.close();
            streamOut.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
