package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.EOFException;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.function.Consumer;
import java.time.LocalTime;

import client.view.ClientGUI;
import commun.*;
import commun.element.*;
import javafx.application.Platform;

import static commun.NetCode.*;

public class Client {
    private Socket socket;
    private static Scanner sc;

    private DataInputStream streamIn;
    private DataOutputStream streamOut;

    private final Hashtable<Integer, Consumer<String[]>> commands = new Hashtable<>();

    private ClientGUI gui;
    private PHuman player;
    private Game game;
    private String gameId;
    private boolean gameCreation = false;
    private final boolean isGUImode;
    private final ArrayList<String[]> gamesStrings = new ArrayList<String[]>();
    private int nbRefused = 0;
    private int coordLRbp, coordCRbp;

    public Client(int port, ClientGUI gui, boolean isGUImode) throws IOException {
        player = new PHuman();
        player.setPseudo(player.generateUniquePlayerId());
        socket = new Socket("localhost", port);
        streamOut = new DataOutputStream(socket.getOutputStream());
        streamOut.flush();
        streamIn = new DataInputStream(socket.getInputStream());
        game = null;
        this.gui = gui;
        this.isGUImode = isGUImode;
    }

    {
        commands.put(LOGIN_ANS, this::loginAns);
        commands.put(ERR_ALREADY_USED, this::displayError);
        commands.put(ERR_TOO_LONG_LOGIN, this::displayError);
        commands.put(MAP_CREATED, this::createMapAns);
        commands.put(GET_MAP_LIST_ANS, this::requestMapListAnswer);
        commands.put(MAP_JOINED, this::joinMapAnswer);
        commands.put(PLAYER_JOINED_BROADCAST, this::playerJoinedBroadcast);
        commands.put(REQUEST_START_GAME, this::requestReceive);
        commands.put(GAME_STARTED_BROADCAST, this::gameStarted);
        commands.put(GAME_ABORTED_BROADCAST, this::gameAborted);
        commands.put(GET_HOLES_ANS, this::getHolesAns);
        commands.put(GET_WALLS_ANS, this::getWallsAns);
        commands.put(GET_TREASURES_ANS, this::getTreasuresAns);
        commands.put(UPDATE_POSITION_BROADCAST, this::updatePlayerPosition);
        commands.put(TREASURE_OPENED_BROADCAST, this::treasureOpened);
        commands.put(MOVE_OK, this::moveOK);
        commands.put(MOVE_OK_TREASURE, this::moveOkTreasure);
        commands.put(MOVE_BLOCKED, this::moveBlocked);
        commands.put(MOVE_DEAD, this::moveDead);
        commands.put(PLAYER_DIED_BROADCAST, this::playerDiedBroadcast);
        commands.put(GAME_END_BROADCAST, this::gameEndBroadcast);
        commands.put(PLAYER_TURN, this::playerTurn);
        commands.put(NOT_YOUR_TURN, this::notYourTurn);
        commands.put(HOLE_PAYMENT_VALIDATED, this::holePayment);
        commands.put(MAP_PAYMENT_VALIDATED, this::mapPayment);
        commands.put(SENDING_HOLES, this::getHolesAns);
        commands.put(SENDING_WALL, this::getWallsAns);
        commands.put(SENDING_TRES, this::getTreasuresAns);
        commands.put(NOT_ENOUGH_POINTS, this::displayError);
    }

    private void loginAns(String[] infos) {
        String fullName = infos[2];
        log("logged in as " + fullName);
        // String[] data = infos[2].split("/");
        // String pseudo = data[0];
        // String heroName = data.length == 1 ? "elf" : data[1];
        this.player.setFullName(fullName);
        if (isGUImode)
            gui.loginAccepted();
    }

    private void createMapAns(String[] infos) {
        this.gameId = infos[3];
        this.game = new Game(this.gameId);
        this.gameCreation = true;
        log("new map created with id " + infos[3]);
        if (isGUImode)
            gui.confirmMapCreated();
    }

    private void joinMapAnswer(String[] parts) {
        this.gameId = parts[2];
        this.game = new Game(this.gameId);
        log("map joined successfully");
        this.game.addPlayer(this.player);
        if (isGUImode)
            gui.joinResponse();

    }

    private boolean leaveMapAnswer() {
        log("map leaved successfully");
        return true;
    }

    private void playerJoinedBroadcast(String[] parts) {
        String fullName = parts[1];
        log(fullName + " has joined the game");
        PHuman player = new PHuman(fullName);
        this.game.addPlayer(player);
        try {
            streamOut.writeUTF(CommandBuilder.ack(fullName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void requestReceive(String parts[]) {
        gameCreation = true;
        log("The creator request starting the game");
        if (isGUImode) {
            gui.setGameStartRequested(true);
        }

    }

    private void gameStarted(String[] parts) {
        log("GAME STARTED");
        this.game.addPlayer(this.player);
        this.gameCreation = false;
        this.displayBoard();
        if (isGUImode) {
            gui.setGameStarted(true);
            gui.initGameView();
        }
    }

    private void gameAborted(String[] parts) {
        StringBuilder data = new StringBuilder();
        if (parts[1].equals("START")) {
            log(parts[3] + " players left the game");
            nbRefused = Integer.parseInt(parts[3]);
        } else {
            data.append(nbRefused).append(" players left the game:\n");
            for (int i = 4; i < parts.length; i++) {
                log(parts[i] + " left the game");
                data.append(parts[i]).append("\n");
            }
            gui.gameAborted(data);
        }
    }

    private void requestMapListAnswer(String[] parts) {
        String complement = parts[1];
        if (complement.toUpperCase().equals("NUMBER")) {
            int numberOfGames = Integer.parseInt(parts[2]);
            log("There is : " + numberOfGames);
            gamesStrings.clear();
        } else {
            if (this.gameCreation) {
                if (parts[4].equals(this.gameId)) {
                    this.game = new Game(
                            new Board(Integer.parseInt(parts[6]), Integer.parseInt(parts[7]), 0,
                                    Integer.parseInt(parts[9]), Integer.parseInt(parts[8]), false),
                            GameMode.values()[Integer.parseInt(parts[5]) - 1]);
                }
            } else {
                String gameDescription = "GAME: " + parts[2] + " ID: " + parts[4] + " MODE: " + parts[5] + " S|H|T : ( "
                        + parts[6] + " , " + parts[7] + " ) | " + parts[8] + " | " + parts[9];
                log(gameDescription);
                if (isGUImode) {
                    gamesStrings.add(parts);
                    gui.showListOfGames();
                }
            }
        }
    }

    private void getHolesAns(String[] parts) {
        if (parts[1].equals("NUMBER")) {
            // TODO: do something here.
            if (Integer.parseInt(parts[0]) == SENDING_HOLES) {
                log("You have " + parts[2] + " in front of you");
                if (isGUImode)
                    gui.showNbOfHolesWarFog(Integer.parseInt(parts[2]));
            } else {

            }
        } else {
            if (this.gameCreation || this.game.getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue()) {
                int l, c;
                for (int i = 4; i < parts.length; i += 2) {
                    l = Integer.parseInt(parts[i]);
                    c = Integer.parseInt(parts[i + 1]);
                    this.game.getBoard().setElement(l, c, new Hole(l, c));
                }
            }
        }
        if (!this.gameCreation && Integer.parseInt(parts[0]) == SENDING_HOLES) {
            displayBoard();
            if (isGUImode) {
                gui.refreshBoard();
            }
        } /// used to print the necessaries things
          /// for mode warfrog.
    }

    private void getWallsAns(String[] parts) {
        if (parts[1].equals("NUMBER")) {
            if (this.gameCreation) {
                this.game.getBoard().setNbWalls(Integer.valueOf(parts[2]));
            }

        } else {
            if (this.gameCreation || this.game.getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue()) {
                int l, c;
                for (int i = 4; i < parts.length; i += 2) {
                    l = Integer.valueOf(parts[i]);
                    c = Integer.valueOf(parts[i + 1]);
                    this.game.getBoard().setElement(l, c, new Wall(l, c));
                }
            }
        }
    }

    private void getTreasuresAns(String[] parts) {
        if (parts[1].equals("NUMBER")) {
            // TODO : do something here.

        } else {
            if (this.gameCreation || this.game.getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue()) {
                int l, c, v;
                for (int i = 4; i < parts.length; i += 3) {
                    l = Integer.valueOf(parts[i]);
                    c = Integer.valueOf(parts[i + 1]);
                    v = Integer.valueOf(parts[i + 2]);
                    this.game.getBoard().setElement(l, c, new Treasure(v, l, c));
                }
            }
        }
    }

    private void updatePlayerPosition(String[] parts) {
        String fullName = parts[1];
        // String[] data = parts[1].split("/");
        // String pseudo = data[0];
        // String heroName = data.length == 1 ? "elf" : data[1];
        if (this.gameCreation) {
            Player p;

            int l = Integer.valueOf(parts[3]);
            int c = Integer.valueOf(parts[4]);

            if (this.player.getFullName().equals(fullName)) {
                p = this.player;
            } else {
                p = new PHuman(fullName);
                this.game.addPlayer(p);
            }

            // this.game.getBoard().setElement(p.getL(), p.getC(), null);
            this.game.getBoard().setElement(l, c, p);
            p.changePosition(l, c);

        } else {

            Player p;
            if (this.player.getFullName().equals(fullName)) {
                p = this.player;
                if (this.game.getGameMode() == 2) {
                    for (Player pl : this.game.getPlayers()) {
                        this.game.getBoard().setElement(pl.getL(), pl.getC(), null);
                    }
                    this.game.getPlayers().clear();
                    this.game.addPlayer(p);
                }
            } else {
                p = this.game.getPlayerByFullName(fullName);

            }
            int l = Integer.valueOf(parts[3]);
            int c = Integer.valueOf(parts[4]);
            if (p == null) {
                p = new PHuman(fullName);
            }
            if (this.game.getGameMode() == 2)
                this.game.addPlayer(p);

            System.out.println("corrds : " + p.getL() + " " + p.getC() + " | " + l + " " + c);
            this.game.getBoard().setElement(p.getL(), p.getC(), null);
            this.game.getBoard().setElement(l, c, p);
            p.changePosition(l, c);

            this.displayBoard();
            if (isGUImode)
                gui.refreshBoard();

            try {
                streamOut.writeUTF(UPDATED_POSITION_RESPONSE + " " + fullName + " UPDATED");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void treasureOpened(String[] parts) {
        String fullName = parts[1];
        // String[] data = parts[1].split("/");
        // String pseudo = data[0];
        // String heroName = data.length == 1 ? "elf" : data[1];

        Player p = this.game.getPlayerByFullName(fullName);
        if (p == null) {
            p = new PHuman(fullName);
            this.game.addPlayer(p);
        }

        int l = Integer.valueOf(parts[3]);
        int c = Integer.valueOf(parts[4]);
        int v = Integer.valueOf(parts[6]);
        this.game.getBoard().setElement(l, c, p);
        this.game.getBoard().setElement(p.getL(), p.getC(), null);
        p.changePosition(l, c);
        if (!this.player.equals(p))
            this.game.changePlayerScore(p, v);
        log(fullName + " has oppened treasure");
        this.displayBoard();
        if (isGUImode)
            gui.refreshBoard();
    }

    private void playerDiedBroadcast(String[] parts) {
        String fullName = parts[1];
        this.game.removePlayer(this.game.getPlayerByFullName(fullName));
        log(parts[1] + " is dead");
        this.displayBoard();
        if (isGUImode && !this.player.getFullName().equals(fullName))
            gui.refreshBoard();
    }

    private void moveOK(String[] parts) {
        log("Your move is validated");
        if (this.game.getGameMode() == 2) {
            if (this.player.hasRbpHelp())
                this.player.getRbpHelp().decreaseRounds();
            if (this.player.hasRtHelp())
                this.player.getRtHelp().decreaseRounds();
        }
        // gui.refereshBoard();
    }

    private void moveOkTreasure(String[] parts) {
        String scoreString = parts[4];
        this.player.addScore(Integer.valueOf(scoreString));
        log("You gained " + Integer.valueOf(scoreString));
        if (this.game.getGameMode() == 2) {
            if (this.player.hasRbpHelp())
                this.player.getRbpHelp().decreaseRounds();
            if (this.player.hasRtHelp())
                this.player.getRtHelp().decreaseRounds();
        }
        if (isGUImode) {
            gui.incrementPlayerScoreDisplay();
        }
    }

    private void moveBlocked(String[] parts) {
        log("Your move is unavailable");
        if (this.game.getGameMode() == 2) {
            if (this.player.hasRbpHelp())
                this.player.getRbpHelp().decreaseRounds();
            if (this.player.hasRtHelp())
                this.player.getRtHelp().decreaseRounds();
        }
        if (isGUImode)
            Platform.runLater(() -> gui.showInformationDialogue("Move Blocked", "Your move is unavailable"));
    }

    private void moveDead(String[] parts) {
        log("You are dead");
        if (isGUImode)
            Platform.runLater(() -> gui.gameOver(true, false, getPlayer().getFullName()));
        resetData();
    }

    private void gameEndBroadcast(String[] parts) {
        log(parts[1] + " WINS THE GAME");
        if (isGUImode) {
            this.gui.resetGUIdata();
            if (parts[1].equals(getPlayer().getFullName())) {
                Platform.runLater(() -> gui.gameOver(false, true, getPlayer().getFullName()));
            } else
                Platform.runLater(() -> gui.gameOver(false, false, parts[1]));
        }
        try {
            streamOut.writeUTF(GAME_OVER + " GAME OVER");
            resetData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void playerTurn(String[] parts) {
        log("It's " + parts[1] + " TURN");
        try {
            streamOut.writeUTF(TURN_UPDATED + " TURN UPDATED");
            if (isGUImode)
                gui.showTurn(parts[1]);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void notYourTurn(String[] parts) {
        log("Not Your Turn");
        if (isGUImode)
            Platform.runLater(() -> gui.showInformationDialogue("TURN", "It's not your turn!"));
    }

    private void holePayment(String[] parts) {
        RevealTraps help = new RevealTraps(game.getBoard(), player);
        player.setRtHelp(help);
        player.pay(20);
        if (isGUImode) {
            gui.refreshBoard();
            gui.incrementPlayerScoreDisplay();
        }
    }

    private void mapPayment(String[] parts) {
        RevealBoardPart help = new RevealBoardPart(this.game.getBoard(), this.player, this.coordLRbp, this.coordCRbp);
        player.setRbpHelp(help);
        player.pay(30);
        if (isGUImode) {
            gui.refreshBoard();
            gui.incrementPlayerScoreDisplay();
        }
    }

    public void initThreadForGUI() {

        Thread receiver = new Thread(new Runnable() {
            String cmd;

            @Override
            public void run() {
                // while (client.streamIn.available() != 0) {
                while (true) {
                    try {
                        cmd = streamIn.readUTF();
                        applyCommand(cmd);
                    } catch (SocketException e) {
                        log("SERVER DISCONNECTED");
                        e.printStackTrace();
                        break;
                    } catch (EOFException e) {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                close();
                System.exit(0);
            }
        });
        receiver.start();

    }

    public static void initThreadForTUI() {
        sc = new Scanner(System.in);

        try {
            // default name = test if main has no arguments
            Client client = new Client(5000, null, false);
            Thread sender = new Thread(new Runnable() {
                String cmd;

                @Override
                public void run() {
                    while (true) {
                        try {
                            cmd = sc.nextLine();
                            client.streamOut.writeUTF(cmd);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            sender.start();

            Thread receiver = new Thread(new Runnable() {
                String cmd;

                @Override
                public void run() {
                    // while (client.streamIn.available() != 0) {
                    while (true) {
                        try {
                            cmd = client.streamIn.readUTF();
                            System.out.println(cmd);
                            client.applyCommand(cmd);
                        } catch (EOFException e) {
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    client.close();
                    System.exit(0);
                }
            });
            receiver.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void applyCommand(String cmd) throws IOException {
        String[] parts = cmd.split(" ");
        int commande = 0;
        try {
            commande = Integer.parseInt(parts[0]);
        } catch (Exception e) {
            System.out.println("applycommand Client 508 not parsable: find why");
            e.printStackTrace();
        }
        if (!commands.containsKey(commande)) {
            log("we haven't implemented the method yet " + commande);
        } else {
            commands.get(commande).accept(parts);
        }
    }

    private void displayBoard() {
        if (this.game.getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue()) {
            System.out.println(this.game.getBoard().displayBrouillardDeGuerre(this.player));
        } else {
            System.out.println(this.game.getBoard().displaySpeedingContest_TourParTour());
        }
    }

    private void displayError(String parts[]) {
        log(parts[1]);
        if (isGUImode)
            Platform.runLater(() -> gui.showInformationDialogue(parts[0], parts[1]));
    }

    public PHuman getPlayer() {
        return player;
    }

    public void log(String prompt) {
        System.out.println("[client " + LocalTime.now().toString() + "] " + prompt);
    }

    public void close() {
        log("disconnected");
        try {
            streamIn.close();
            streamOut.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DataInputStream getStreamIn() {
        return streamIn;
    }

    private void resetData() {
        this.game = null;
        this.gameId = "";
        gameCreation = false;
        gamesStrings.clear();
        String fullname = player.getFullName();
        this.player = new PHuman();
        this.player.setFullName(fullname);
        nbRefused = 0;
    }

    public DataOutputStream getStreamOut() {
        return streamOut;
    }

    public Game getGame() {
        return game;
    }

    public ArrayList<String[]> getGamesStrings() {
        return gamesStrings;
    }

    public ClientGUI getGui() {
        return gui;
    }

    public void setCoordCRbp(int coordCRbp) {
        this.coordCRbp = coordCRbp;
    }

    public void setCoordLRbp(int coordLRbp) {
        this.coordLRbp = coordLRbp;
    }
}
