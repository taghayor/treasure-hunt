package client.view.multiscene.controller;

import client.Client;
import client.view.ClientGUI;
import client.view.multiscene.model.ClientHolder;
import client.view.multiscene.model.SceneName;
import client.view.multiscene.model.Stageable;
import commun.CommandBuilder;
import commun.NetCode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The controller for in-game.fxml This class handles all the graphics in the
 * game screen and sends client requests
 */
public class InGameController implements Stageable, ClientHolder, Initializable {

    private Stage stage;
    private Client client;
    private ClientGUI gui;

    /**
     * Helps to know if the showMapPart help in WarFog mode is on and if the zone
     * rectangle should be drawn
     */
    private boolean revealPartHelpOn;
    /**
     * Stored for using in {@code buyMapPart()} in WarFog mode
     */
    private int iBoxChosen, jBoxChosen;

    @FXML
    private Pane canvasContainer;

    @FXML
    private Label backButton;
    @FXML
    private Label buyHolesButton, buyMapPartButton/* , cancelHelp */;
    @FXML
    private Label turnLabel, scoreLabel;

    /**
     * Opens the buyHoleHelp dialogue and sends the request to server if ok was
     * selected.
     */
    @FXML
    void buyHolesButtonClicked(MouseEvent event) {
        Optional<ButtonType> buyHolesAns = gui.showOptionalDialogue("Buy Holes View",
                "Do you want to spend 20 coins to see holes around you?");
        if (buyHolesAns.get() == ButtonType.OK) {
            if (client.getPlayer().getPlayerScore() >= 20) {
                try {
                    client.getStreamOut().writeUTF(CommandBuilder.revealHole());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                gui.showInformationDialogue(NetCode.NOT_ENOUGH_POINTS + "", "Sorry, you don't have enough coins:(");
            }
        }
    }

    @FXML
    void buyHolesButtonEntered(MouseEvent event) {
        buyHolesButton.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void buyHolesButtonExited(MouseEvent event) {
        buyHolesButton.setEffect(null);

    }

    /**
     * Opens the buyHoleHelp dialogue and sends the request to server if ok was
     * selected.
     */
    @FXML
    void buyMapPartButtonClicked(MouseEvent event) {
        Optional<ButtonType> buyHolesAns = gui.showOptionalDialogue("Buy Map Part View",
                "Do you want to spend 30 coins to see a part of the map?");
        if (buyHolesAns.get() == ButtonType.OK) {
            if (client.getPlayer().getPlayerScore() >= 30) {
                revealPartHelpOn = true;
                // cancelHelp.setVisible(true);
            } else {
                gui.showInformationDialogue(NetCode.NOT_ENOUGH_POINTS + "", "Sorry, you don't have enough coins:(");
            }
        }
    }

    @FXML
    void buyMapPartButtonEntered(MouseEvent event) {
        buyMapPartButton.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void buyMapPartButtonExited(MouseEvent event) {
        buyMapPartButton.setEffect(null);
    }

    @FXML
    void moveWithKeyListener(KeyEvent event) {

        if (event.getCode() == KeyCode.DOWN || event.getCharacter().equals("s")) {
            try {
                String command = CommandBuilder.goDown();
                client.getStreamOut().writeUTF(command);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (event.getCode() == KeyCode.RIGHT || event.getCharacter().equals("d")) {
            try {
                String command = CommandBuilder.goRight();
                client.getStreamOut().writeUTF(command);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (event.getCode() == KeyCode.UP || event.getCharacter().equals("z")) {
            try {
                String command = CommandBuilder.goUp();
                client.getStreamOut().writeUTF(command);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (event.getCode() == KeyCode.LEFT || event.getCharacter().equals("q")) {
            try {
                String command = CommandBuilder.goLeft();
                client.getStreamOut().writeUTF(command);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Calculates the the upper left vertice of the help zone rectangle and redraws.
     * <br>
     * For more efficency it redraws if the {@code iBoxChosen} or {@code jBoxChosen}
     * have changed.
     */
    @FXML
    void mouseMoved(MouseEvent event) {
        if (revealPartHelpOn) {
            int boardWidth = client.getGame().getBoard().getWidth();
            int boardHeight = client.getGame().getBoard().getHeight();
            int boxSize = gui.getBoxSize(), startX = gui.getStartX(), startY = gui.getStartY();
            if (event.getX() > startX + boxSize && event.getY() > startY + boxSize
                    && event.getX() < startX + (boardWidth - 1) * boxSize
                    && event.getY() < startY + (boardHeight - 1) * boxSize) {
                int iChosen = (int) ((event.getX() - startX) / boxSize), jChosen = (int) ((event.getY() - startY) / boxSize);
                if (iChosen != this.iBoxChosen || jChosen != this.jBoxChosen) {
                    this.iBoxChosen = iChosen;
                    this.jBoxChosen = jChosen;
                    int x = iChosen * boxSize + startX;
                    int y = jChosen * boxSize + startY;
                    ClientGUI.ResizableCanvas canvas = (ClientGUI.ResizableCanvas) canvasContainer.getChildren().get(0);
                    canvas.draw(x, y, 3 * boxSize, 3 * boxSize);
                }
            }
        }
    }

    /**
     * Calculates {@code iBoxChosen} and {@code jBoxChosen} and sends the revealMap
     * request to server.
     */
    @FXML
    void mousePressed(MouseEvent event) {
        if (revealPartHelpOn) {
            int boardWidth = client.getGame().getBoard().getWidth();
            int boardHeight = client.getGame().getBoard().getHeight();
            int boxSize = gui.getBoxSize(), startX = gui.getStartX(), startY = gui.getStartY();
            if (event.getX() > startX + boxSize && event.getY() > startY + boxSize
                    && event.getX() < startX + (boardWidth - 1) * boxSize
                    && event.getY() < startY + (boardHeight - 1) * boxSize) {
                // cancelHelp.setVisible(false);
                revealPartHelpOn = false;
                // tmpCoinForCancel = client.getPlayer().getPlayerScore();
                try {
                    client.setCoordLRbp(jBoxChosen);
                    client.setCoordCRbp(iBoxChosen);
                    client.getStreamOut().writeUTF(CommandBuilder.revealMap(jBoxChosen, iBoxChosen));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                gui.refreshBoard();
            }

        }

    }

    // @FXML
    // void cancelHelpClicked(MouseEvent event) {
    // reavealPartHelpOn = false;
    // cancelHelp.setVisible(false);
    // }
    //
    // @FXML
    // void cancelHelpEntered(MouseEvent event) {
    // cancelHelp.setEffect(new DropShadow(3, Color.WHITE));
    //
    // }
    //
    // @FXML
    // void cancelHelpExited(MouseEvent event) {
    // cancelHelp.setEffect(null);
    // }

    @FXML
    void goBackToLobbyAndExitGame(MouseEvent event) {
        Optional<ButtonType> exitgame = gui.showOptionalDialogue("Exit game",
                "Are you sure you want to exit the game?");
        if (exitgame.get() == ButtonType.OK)
            stage.setScene(gui.getScenes().get(SceneName.LOBBY).getScene());

    }

    @FXML
    void backButtonEntered(MouseEvent event) {
        backButton.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void backButtonExited(MouseEvent event) {
        backButton.setEffect(null);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setClient(Client client) {
        this.client = client;
        this.gui = client.getGui();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.scoreLabel.setText("Coins 0");
    }

}
