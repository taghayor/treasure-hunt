package client.view.multiscene.controller;

import client.Client;
import client.view.ClientGUI;
import client.view.multiscene.model.ClientHolder;
import client.view.multiscene.model.SceneName;
import client.view.multiscene.model.Stageable;
import commun.CommandBuilder;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The controller for choose-character.fxml
 * This class handles all the graphics in Choosing Character screen and sends client requests
 */
public class ChooseCharacterController implements Stageable, ClientHolder, Initializable {

    private Stage stage;
    private Client client;
    private ClientGUI gui;

    private static final Color DEFAULT_REC_COLOR = new Color(0.125, 0.125, 0.125, 1);
    ;
    @FXML
    private Rectangle mushroomRec, trollRec, bruteRec, fairyRec, elfRec, wizardRec, forestGaurdianRec, rangerRec, entRec, golemRec, knightRec, clericRec;
    @FXML
    private ImageView mushroomImage, trollImage, bruteImage, fairyImage, elfImage, wizardImage, guardianImage, rangerImage, entImage, golemImage, knightImage, clericImage;
    @FXML
    private Label backLabel;

    @FXML
    void backButtonEntered(MouseEvent event) {
        backLabel.setEffect(new DropShadow(3, Color.WHITE));

    }

    @FXML
    void backButtonExited(MouseEvent event) {
        backLabel.setEffect(null);
    }

    @FXML
    void goBackToLoginScreen(MouseEvent event) {
        backLabel.setEffect(new DropShadow(3, Color.GREY));
        stage.setScene(gui.getScenes().get(SceneName.LOGIN_MENU).getScene());
    }


    @FXML
    void bruteImageClicked(MouseEvent event) {
        bruteRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("brute");
    }

    @FXML
    void bruteImagePressed(MouseEvent event) {
        bruteRec.setFill(Color.CHARTREUSE);

    }

    @FXML
    void bruteImageEntered(MouseEvent event) {
        bruteRec.setFill(Color.GOLD);
        bruteRec.setWidth(200);
        bruteRec.setHeight(200);
        bruteImage.setFitHeight(155);

    }

    @FXML
    void bruteImageExited(MouseEvent event) {
        bruteRec.setFill(DEFAULT_REC_COLOR);
        bruteRec.setWidth(188);
        bruteRec.setHeight(164);
        bruteImage.setFitHeight(131);
    }

    @FXML
    void clericImageClicked(MouseEvent event) {
        clericRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("cleric");
    }

    @FXML
    void clericImagePressed(MouseEvent event) {
        clericRec.setFill(Color.CHARTREUSE);

    }

    @FXML
    void clericImageEntered(MouseEvent event) {
        clericRec.setFill(Color.GOLD);
        clericRec.setWidth(200);
        clericRec.setHeight(200);
        clericImage.setFitHeight(175);
        clericImage.setFitWidth(175);


    }

    @FXML
    void clericImageExited(MouseEvent event) {
        clericRec.setFill(DEFAULT_REC_COLOR);
        clericRec.setWidth(188);
        clericRec.setHeight(164);
        clericImage.setFitHeight(144);
        clericImage.setFitWidth(152);
    }

    @FXML
    void elfImageClicked(MouseEvent event) {
        elfRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("elf");
    }

    @FXML
    void elfImagePressed(MouseEvent event) {
        elfRec.setFill(Color.CHARTREUSE);

    }

    @FXML
    void elfImageEntered(MouseEvent event) {
        elfRec.setFill(Color.GOLD);
        elfRec.setWidth(200);
        elfRec.setHeight(200);
        elfImage.setFitWidth(160);

    }

    @FXML
    void elfImageExited(MouseEvent event) {
        elfRec.setFill(DEFAULT_REC_COLOR);
        elfRec.setWidth(188);
        elfRec.setHeight(164);
        elfImage.setFitWidth(140);
    }

    @FXML
    void entImageClicked(MouseEvent event) {
        entRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("ent");
    }

    @FXML
    void entImagePressed(MouseEvent event) {
        entRec.setFill(Color.CHARTREUSE);

    }

    @FXML
    void entImageEntered(MouseEvent event) {
        entRec.setFill(Color.GOLD);
        entRec.setWidth(200);
        entRec.setHeight(200);
        entImage.setFitHeight(170);

    }

    @FXML
    void entImageExited(MouseEvent event) {
        entRec.setFill(DEFAULT_REC_COLOR);
        entRec.setWidth(188);
        entRec.setHeight(164);
        entImage.setFitHeight(154);
    }

    @FXML
    void fairyImageClicked(MouseEvent event) {
        fairyRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("fairy");
    }

    @FXML
    void fairyImagePressed(MouseEvent event) {
        fairyRec.setFill(Color.CHARTREUSE);

    }

    @FXML
    void fairyImageEntered(MouseEvent event) {
        fairyRec.setFill(Color.GOLD);
        fairyRec.setWidth(200);
        fairyRec.setHeight(200);
        fairyImage.setFitWidth(170);

    }

    @FXML
    void fairyImageExited(MouseEvent event) {
        fairyRec.setFill(DEFAULT_REC_COLOR);
        fairyRec.setWidth(188);
        fairyRec.setHeight(164);
        fairyImage.setFitWidth(148);
    }


    @FXML
    void golemImageClicked(MouseEvent event) {
        golemRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("golem");
    }

    @FXML
    void golemImagePressed(MouseEvent event) {
        golemRec.setFill(Color.CHARTREUSE);

    }

    @FXML
    void golemImageEntered(MouseEvent event) {
        golemRec.setFill(Color.GOLD);
        golemRec.setWidth(200);
        golemRec.setHeight(200);
        golemImage.setFitWidth(185);
        golemImage.setFitHeight(185);

    }

    @FXML
    void golemImageExited(MouseEvent event) {
        golemRec.setFill(DEFAULT_REC_COLOR);
        golemRec.setWidth(188);
        golemRec.setHeight(164);
        golemImage.setFitWidth(163);
        golemImage.setFitHeight(163);
    }

    @FXML
    void guardianImageClicked(MouseEvent event) {
        forestGaurdianRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("guardian");
    }

    @FXML
    void guardianImagePressed(MouseEvent event) {
        forestGaurdianRec.setFill(Color.CHARTREUSE);

    }


    @FXML
    void guardianImageEntered(MouseEvent event) {
        forestGaurdianRec.setFill(Color.GOLD);
        forestGaurdianRec.setWidth(200);
        forestGaurdianRec.setHeight(200);
        guardianImage.setFitWidth(200);
        guardianImage.setFitHeight(180);


    }

    @FXML
    void guardianImageExited(MouseEvent event) {
        forestGaurdianRec.setFill(DEFAULT_REC_COLOR);
        forestGaurdianRec.setWidth(188);
        forestGaurdianRec.setHeight(164);
        guardianImage.setFitWidth(159);
        guardianImage.setFitHeight(164);
    }


    @FXML
    void knightImageClicked(MouseEvent event) {
        knightRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("knight");
    }

    @FXML
    void knightImagePressed(MouseEvent event) {
        knightRec.setFill(Color.CHARTREUSE);

    }

    @FXML
    void knightImageEntered(MouseEvent event) {
        knightRec.setFill(Color.GOLD);
        knightRec.setWidth(200);
        knightRec.setHeight(200);
        knightImage.setFitHeight(175);

    }

    @FXML
    void knightImageExited(MouseEvent event) {
        knightRec.setFill(DEFAULT_REC_COLOR);
        knightRec.setWidth(188);
        knightRec.setHeight(164);
        knightImage.setFitHeight(152);
    }

    @FXML
    void mushroomImageClicked(MouseEvent event) {
        mushroomRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("mushroom");
    }

    @FXML
    void mushroomImagePressed(MouseEvent event) {
        mushroomRec.setFill(Color.CHARTREUSE);

    }

    @FXML
    void mushroomImageEntered(MouseEvent event) {
        mushroomRec.setFill(Color.GOLD);
        mushroomRec.setWidth(200);
        mushroomRec.setHeight(200);
        mushroomImage.setFitHeight(170);
    }

    @FXML
    void mushroomImageExited(MouseEvent event) {
        mushroomRec.setFill(DEFAULT_REC_COLOR);
        mushroomRec.setWidth(188);
        mushroomRec.setHeight(164);
        mushroomImage.setFitHeight(151);
    }

    @FXML
    void rangerImageClicked(MouseEvent event) {
        rangerRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("ranger");
    }

    @FXML
    void rangerImagePressed(MouseEvent event) {
        rangerRec.setFill(Color.CHARTREUSE);

    }


    @FXML
    void rangerImageEntered(MouseEvent event) {
        rangerRec.setFill(Color.GOLD);
        rangerRec.setWidth(200);
        rangerRec.setHeight(200);
        rangerImage.setFitHeight(160);
    }

    @FXML
    void rangerImageExited(MouseEvent event) {
        rangerRec.setFill(DEFAULT_REC_COLOR);
        rangerRec.setWidth(188);
        rangerRec.setHeight(164);
        rangerImage.setFitHeight(145);

    }

    @FXML
    void trollImageClicked(MouseEvent event) {
        trollRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("troll");
    }

    @FXML
    void trollImagePressed(MouseEvent event) {
        trollRec.setFill(Color.CHARTREUSE);

    }


    @FXML
    void trollImageEntered(MouseEvent event) {
        trollRec.setFill(Color.GOLD);
        trollRec.setWidth(200);
        trollRec.setHeight(200);
        trollImage.setFitHeight(180);
        trollImage.setFitWidth(200);

    }

    @FXML
    void trollImageExited(MouseEvent event) {
        trollRec.setFill(DEFAULT_REC_COLOR);
        trollRec.setWidth(188);
        trollRec.setHeight(164);
        trollImage.setFitHeight(164);
        trollImage.setFitWidth(187);
    }

    @FXML
    void wizardImageClicked(MouseEvent event) {
        wizardRec.setFill(DEFAULT_REC_COLOR);
        sendLoginRequest("wizard");
    }

    @FXML
    void wizardImagePressed(MouseEvent event) {
        wizardRec.setFill(Color.CHARTREUSE);
    }


    @FXML
    void wizardImageEntered(MouseEvent event) {
        wizardRec.setFill(Color.GOLD);
        wizardRec.setWidth(200);
        wizardRec.setHeight(200);
        wizardImage.setFitHeight(180);
        wizardImage.setFitWidth(180);

    }

    @FXML
    void wizardImageExited(MouseEvent event) {
        wizardRec.setFill(DEFAULT_REC_COLOR);
        wizardRec.setWidth(188);
        wizardRec.setHeight(164);
        wizardImage.setFitHeight(159);
    }


    private void sendLoginRequest(String heroName) {
        try {
            if (client.getPlayer().getPseudo().equals(gui.getPseudo()) && client.getPlayer().getHeroName().equals(heroName))
                stage.setScene(gui.getScenes().get(SceneName.LOBBY).getScene());
            else {
                String command = CommandBuilder.hello(gui.getPseudo() + "/" + heroName);
                client.getStreamOut().writeUTF(command);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setClient(Client client) {
        this.client = client;
        this.gui = client.getGui();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
