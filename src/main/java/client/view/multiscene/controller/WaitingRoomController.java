package client.view.multiscene.controller;

import client.Client;
import client.view.ClientGUI;
import client.view.multiscene.model.ClientHolder;
import client.view.multiscene.model.SceneName;
import client.view.multiscene.model.Stageable;
import commun.Board;
import commun.CommandBuilder;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The controller for waiting-room.fxml
 * This class handles all the graphics in the waiting room menu and sends client requests
 */
public class WaitingRoomController implements Initializable, Stageable, ClientHolder {

    private Stage stage;
    private Client client;

    @FXML
    public Label acceptLabel, startLabel, refuseLabel;

    /**
     * Sends accept starting game request
     */
    @FXML
    void accept(MouseEvent event) {
        try {
            String command = CommandBuilder.start("YES");
            client.getStreamOut().writeUTF(command);
            acceptLabel.setVisible(false);
            refuseLabel.setVisible(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends refuse starting game request
     */
    @FXML
    void refuse(MouseEvent event) {
        try {
            String command = CommandBuilder.start("NO");
            client.getStreamOut().writeUTF(command);
            startLabel.setVisible(false);
            refuseLabel.setVisible(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends starting game request
     */
    @FXML
    void startGame(MouseEvent event) {

        try {
            String command = CommandBuilder.requestStart();
            client.getStreamOut().writeUTF(command);
            startLabel.setVisible(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void acceptButtonEntered(MouseEvent event) {
        acceptLabel.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void acceptButtonExited(MouseEvent event) {
        acceptLabel.setEffect(null);
    }

    @FXML
    void refuseButtonEntered(MouseEvent event) {
        refuseLabel.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void refuseButtonExited(MouseEvent event) {
        refuseLabel.setEffect(null);
    }

    @FXML
    void startButtonEntered(MouseEvent event) {
        startLabel.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void startButtonExited(MouseEvent event) {
        startLabel.setEffect(null);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        turnOffButtons();
    }

    public void turnOffButtons() {
        startLabel.setVisible(false);
        acceptLabel.setVisible(false);
        refuseLabel.setVisible(false);
    }
}
