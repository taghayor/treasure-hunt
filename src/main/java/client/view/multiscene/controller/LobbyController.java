package client.view.multiscene.controller;

import client.Client;
import client.view.ClientGUI;
import client.view.multiscene.model.ClientHolder;
import client.view.multiscene.model.SceneName;
import client.view.multiscene.model.Stageable;
import commun.CommandBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

/**
 * The controller for lobby.fxml
 * This class handles all the graphics in the lobby screen and sends client requests
 */
public class LobbyController implements Stageable, ClientHolder {

    private Stage stage;
    private Client client;
    private ClientGUI gui;


    @FXML
    private Label createGameButton, joinGameButton, backLabel;


    /**
     * sends request to the server to receive the list of available maps
     */
    @FXML
    void joinGameButtonClicked(MouseEvent event) {

        try {
            String command = CommandBuilder.getlist();
            client.getStreamOut().writeUTF(command);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void createGameButtonClicked(MouseEvent event) { //
        stage.setScene(gui.getScenes().get(SceneName.CREATE_GAME_MENU).getScene());
    }

    @FXML
    void backButtonClicked(MouseEvent event) {
        stage.setScene(gui.getScenes().get(SceneName.CHOOSE_CHARACTER).getScene());

    }


    @FXML
    void backButtonEntered(MouseEvent event) {
        backLabel.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void backButtonExited(MouseEvent event) {
        backLabel.setEffect(null);
    }

    @FXML
    void createGameButtonEntered(MouseEvent event) {
        createGameButton.setEffect(new DropShadow(3, Color.WHITE));

    }

    @FXML
    void createGameButtonExited(MouseEvent event) {
        createGameButton.setEffect(null);
    }


    @FXML
    void joinGameButtonEntered(MouseEvent event) {
        joinGameButton.setEffect(new DropShadow(3, Color.WHITE));

    }

    @FXML
    void joinGameButtonExited(MouseEvent event) {
        joinGameButton.setEffect(null);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setClient(Client client) {
        this.client = client;
        this.gui = client.getGui();
    }
}
