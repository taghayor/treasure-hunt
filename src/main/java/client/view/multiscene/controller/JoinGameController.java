package client.view.multiscene.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import client.Client;
import client.view.ClientGUI;
import client.view.multiscene.model.ClientHolder;
import client.view.multiscene.model.SceneName;
import client.view.multiscene.model.Stageable;
import commun.CommandBuilder;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * The controller for join-game-menu.fxml
 * This class handles all the graphics in the join screen and sends client requests
 */
public class JoinGameController implements Stageable, ClientHolder {
    private Stage stage;
    private Client client;
    private ClientGUI gui;

    @FXML
    private Label joinButton, backButton;
    @FXML
    private ListView<String[]> gamesListView;

    @FXML
    void goBackToLobby(MouseEvent event) {
        stage.setScene(gui.getScenes().get(SceneName.LOBBY).getScene());

    }

    @FXML
    void goBackToLobbyEntered(MouseEvent event) {
        backButton.setEffect(new DropShadow(3, Color.WHITE));

    }

    @FXML
    void goBackToLobbyExited(MouseEvent event) {
        backButton.setEffect(null);
    }

    /**
     * Sends the data of the chosen map for joining to the server
     */
    @FXML
    void joinGameButtonClicked(MouseEvent event) {
        String[] gameDesc = gamesListView.getSelectionModel().getSelectedItem();
        if (gameDesc == null || gameDesc.length == 0)
            return;
        String gameId = gameDesc[4]; // 4 pour ID
        try {
            client.getStreamOut().writeUTF(CommandBuilder.join(gameId));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void joinGameButtonEntered(MouseEvent event) {
        joinButton.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void joinGameButtonExited(MouseEvent event) {
        joinButton.setEffect(null);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setClient(Client client) {
        this.client = client;
        this.gui = client.getGui();
    }
}
