package client.view.multiscene.controller;

import client.Client;
import client.view.ClientGUI;
import client.view.multiscene.model.ClientHolder;
import client.view.multiscene.model.SceneName;
import client.view.multiscene.model.Stageable;
import commun.CommandBuilder;
import commun.GameMode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The controller for create-game-menu.fxml
 * This class handles all the graphics in create game screen and sends client requests
 */

public class CreateGameController implements Stageable, ClientHolder {

    private Stage stage;
    private Client client;
    private ClientGUI gui;
    private boolean mode1Clicked = true;
    private boolean mode2Clicked = false;
    private boolean mode3Clicked = false;
    private int width = 10, height = 10, nbOfHoles = 10, nbOfTreasures = 10;
    private GameMode gameMode = GameMode.SPEEDING_CONTEST;

    @FXML
    private Label mode1, mode2, mode3;
    @FXML
    private Slider sliderWidth, sliderHeight, sliderTreasures, sliderHoles;
    @FXML
    private Label backToLobbyButton, confirmMapButton;

    @FXML
    void goBackToLobby(MouseEvent event) {
        stage.setScene(gui.getScenes().get(SceneName.LOBBY).getScene());

    }

    /**
     * Sends the data of the map that will be created to the server
     */
    @FXML
    void confirmMap(MouseEvent event) {

        try {
            String command = CommandBuilder.create(gameMode, height, width, nbOfHoles, nbOfTreasures);
            client.getStreamOut().writeUTF(command);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void backButtonEntered(MouseEvent event) {
        backToLobbyButton.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void backButtonExited(MouseEvent event) {
        backToLobbyButton.setEffect(null);
    }

    @FXML
    void confirmMapEntered(MouseEvent event) {
        confirmMapButton.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void confirmMapExited(MouseEvent event) {
        confirmMapButton.setEffect(null);
    }


    @FXML
    void getHeightfromSlider(MouseEvent event) {
        if (sliderHeight != null)
            this.height = (int) sliderHeight.getValue();
    }

    @FXML
    void getWidthfromSlider(MouseEvent event) {
        if (sliderWidth != null)
            this.width = (int) sliderWidth.getValue();
    }

    @FXML
    void getnbOFTresfromSlider(MouseEvent event) {
        if (sliderTreasures != null)
            this.nbOfTreasures = (int) sliderTreasures.getValue();
    }

    @FXML
    void getnbOfHolesfromSlider(MouseEvent event) {
        if (sliderHoles != null)
            this.nbOfHoles = (int) sliderHoles.getValue();
    }

    @FXML
    void mode1Entered(MouseEvent event) {
        if (!mode1Clicked) {
            mode1.setEffect(new DropShadow(3, Color.WHITE));
        }
    }

    @FXML
    void mode1Exited(MouseEvent event) {
        if (!mode1Clicked) {
            mode1.setEffect(null);
        }
    }

    @FXML
    void mode1Pressed(MouseEvent event) {
        if (!mode1Clicked) {
            mode1.setTextFill(Color.WHITE);
            mode1.setEffect(new DropShadow(10, Color.WHITE));
            mode1Clicked = true;
            mode2Clicked = false;
            mode3Clicked = false;
            gameMode = GameMode.SPEEDING_CONTEST;
            resetLabelsStyle(1);

        }
    }

    @FXML
    void mode2Entered(MouseEvent event) {
        if (!mode2Clicked) {
            mode2.setEffect(new DropShadow(3, Color.WHITE));
        }
    }

    @FXML
    void mode2Exited(MouseEvent event) {
        if (!mode2Clicked) {
            mode2.setEffect(null);
        }
    }

    @FXML
    void mode2Pressed(MouseEvent event) {
        if (!mode2Clicked) {
            mode2.setTextFill(Color.WHITE);
            mode2.setEffect(new DropShadow(10, Color.WHITE));
            mode2Clicked = true;
            mode1Clicked = false;
            mode3Clicked = false;
            gameMode = GameMode.TOUR_PAR_TOUR;
            resetLabelsStyle(2);

        }
    }

    @FXML
    void mode3Entered(MouseEvent event) {
        if (!mode3Clicked) {
            mode3.setEffect(new DropShadow(3, Color.WHITE));
        }
    }

    @FXML
    void mode3Exited(MouseEvent event) {
        if (!mode3Clicked) {
            mode3.setEffect(null);
        }
    }

    @FXML
    void mode3Pressed(MouseEvent event) {
        if (!mode3Clicked) {
            mode3.setTextFill(Color.WHITE);
            mode3.setEffect(new DropShadow(10, Color.WHITE));
            mode3Clicked = true;
            mode1Clicked = false;
            mode2Clicked = false;
            gameMode = GameMode.BROUILLARD_DE_GUERRE;
            resetLabelsStyle(3);
        }
    }

    private void resetLabelsStyle(int i) {
        switch (i) {
            case 1:
                mode2.setTextFill(Color.GRAY);
                mode3.setTextFill(Color.GRAY);
                mode2.setEffect(null);
                mode3.setEffect(null);
                break;
            case 2:
                mode1.setTextFill(Color.GRAY);
                mode3.setTextFill(Color.GRAY);
                mode1.setEffect(null);
                mode3.setEffect(null);
                break;
            case 3:
                mode1.setTextFill(Color.GRAY);
                mode2.setTextFill(Color.GRAY);
                mode1.setEffect(null);
                mode2.setEffect(null);
                break;
        }
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setClient(Client client) {
        this.client = client;
        this.gui = client.getGui();
    }

}
