package client.view.multiscene.controller;

import client.Client;
import client.view.ClientGUI;
import client.view.multiscene.model.ClientHolder;
import client.view.multiscene.model.SceneName;
import client.view.multiscene.model.Stageable;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * The controller for login-screen.fxml
 * This class handles all the graphics in the login screen and sends client requests
 */
public class LoginScreenController implements Stageable, ClientHolder, Initializable {

    private Stage stage;
    private Client client;
    private ClientGUI gui;

    @FXML
    private ImageView banditThumbnail, knightThumbnail, fairyThumbnail, mushroomThumbnail, golemThumbnail, wizardThumbnail;

    @FXML
    private Label exitLabel, loginLabel, gameLabel;


    @FXML
    private TextField pseudoTextField;


    @FXML
    void loginButtonEntered(MouseEvent event) {
        loginLabel.setEffect(new DropShadow(3, Color.WHITE));

    }

    @FXML
    void loginButtonExited(MouseEvent event) {
        loginLabel.setEffect(null);

    }

    /**
     * Updates the chosen pseudo and stores it in ClientGUI for future use of login request
     */
    @FXML
    void loginGameButtonHandler(MouseEvent event) {
        try {
            if (pseudoTextField.getText() != null && !pseudoTextField.getText().equals("")) {
                loginLabel.setEffect(new DropShadow(3, Color.GRAY));
                String nameTmp = pseudoTextField.getText();
                nameTmp = nameTmp.replaceAll("/", "_"); // to avoid parsing errors
                nameTmp = nameTmp.replaceAll(" ", "_");
                gui.setPseudo(nameTmp);
                stage.setScene(gui.getScenes().get(SceneName.CHOOSE_CHARACTER).getScene());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @FXML
    void exit(MouseEvent event) {
        exitLabel.setEffect(new DropShadow(3, Color.GREY));
        stage.setScene(gui.getScenes().get(SceneName.CHOOSE_CHARACTER).getScene());
        stage.close();
        System.exit(0);
    }


    @FXML
    void exitButtonEntered(MouseEvent event) {
        exitLabel.setEffect(new DropShadow(3, Color.WHITE));
    }

    @FXML
    void exitButtonExited(MouseEvent event) {
        exitLabel.setEffect(null);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setClient(Client client) {
        this.client = client;
        this.gui = client.getGui();
    }

    public Client getClient() {
        return client;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fairyThumbnail.setOpacity(0);
        golemThumbnail.setOpacity(0);
        banditThumbnail.setOpacity(0);
        wizardThumbnail.setOpacity(0);
        knightThumbnail.setOpacity(0);
        mushroomThumbnail.setOpacity(0);

        FadeTransition fadeIn = new FadeTransition(Duration.seconds(1), gameLabel);
        fadeIn.setFromValue(0.2);
        fadeIn.setToValue(1);
        fadeIn.setCycleCount(1);

        FadeTransition fadeOut = new FadeTransition(Duration.seconds(1), gameLabel);
        fadeOut.setFromValue(1);
        fadeOut.setToValue(0.2);
        fadeOut.setCycleCount(1);

        fadeIn.setOnFinished(event -> fadeOut.play());
        fadeOut.setOnFinished(event -> fadeIn.play());
        fadeIn.play();

        FadeTransition fadeInFairy = new FadeTransition(Duration.seconds(2), fairyThumbnail);
        fadeInFairy.setFromValue(0);
        fadeInFairy.setToValue(1);
        fadeInFairy.setCycleCount(1);

        FadeTransition fadeOutFairy = new FadeTransition(Duration.seconds(1), fairyThumbnail);
        fadeOutFairy.setFromValue(1);
        fadeOutFairy.setToValue(0);
        fadeOutFairy.setCycleCount(1);

        FadeTransition fadeInBandit = new FadeTransition(Duration.seconds(2), banditThumbnail);
        fadeInBandit.setFromValue(0);
        fadeInBandit.setToValue(1);
        fadeInBandit.setCycleCount(1);

        FadeTransition fadeOutBandit = new FadeTransition(Duration.seconds(1), banditThumbnail);
        fadeOutBandit.setFromValue(1);
        fadeOutBandit.setToValue(0);
        fadeOutBandit.setCycleCount(1);

        FadeTransition fadeInWizard = new FadeTransition(Duration.seconds(2), wizardThumbnail);
        fadeInWizard.setFromValue(0);
        fadeInWizard.setToValue(1);
        fadeInWizard.setCycleCount(1);

        FadeTransition fadeOutWizard = new FadeTransition(Duration.seconds(1), wizardThumbnail);
        fadeOutWizard.setFromValue(1);
        fadeOutWizard.setToValue(0);
        fadeOutWizard.setCycleCount(1);

        FadeTransition fadeInGolem = new FadeTransition(Duration.seconds(2), golemThumbnail);
        fadeInGolem.setFromValue(0);
        fadeInGolem.setToValue(1);
        fadeInGolem.setCycleCount(1);

        FadeTransition fadeOutGolem = new FadeTransition(Duration.seconds(1), golemThumbnail);
        fadeOutGolem.setFromValue(1);
        fadeOutGolem.setToValue(0);
        fadeOutGolem.setCycleCount(1);

        FadeTransition fadeInKnight = new FadeTransition(Duration.seconds(2), knightThumbnail);
        fadeInKnight.setFromValue(0);
        fadeInKnight.setToValue(1);
        fadeInKnight.setCycleCount(1);

        FadeTransition fadeOutKnight = new FadeTransition(Duration.seconds(1), knightThumbnail);
        fadeOutKnight.setFromValue(1);
        fadeOutKnight.setToValue(0);
        fadeOutKnight.setCycleCount(1);

        FadeTransition fadeInMushroom = new FadeTransition(Duration.seconds(2), mushroomThumbnail);
        fadeInMushroom.setFromValue(0);
        fadeInMushroom.setToValue(1);
        fadeInMushroom.setCycleCount(1);

        FadeTransition fadeOutMushroom = new FadeTransition(Duration.seconds(1), mushroomThumbnail);
        fadeOutMushroom.setFromValue(1);
        fadeOutMushroom.setToValue(0);
        fadeOutMushroom.setCycleCount(1);

        fadeInGolem.setOnFinished(event -> fadeOutGolem.play());
        fadeOutGolem.setOnFinished(event -> {
//            fairyThumbnail.setVisible(true);
            fadeInFairy.play();
        });
        // These are for cute fadeout animations in the first menu:(<3
        fadeInFairy.setOnFinished(event -> fadeOutFairy.play());
        fadeOutFairy.setOnFinished(event -> fadeInBandit.play());
        fadeInBandit.setOnFinished(event -> fadeOutBandit.play());
        fadeOutBandit.setOnFinished(event -> fadeInWizard.play());
        fadeInWizard.setOnFinished(event -> fadeOutWizard.play());
        fadeOutWizard.setOnFinished(event -> fadeInKnight.play());
        fadeInKnight.setOnFinished(event -> fadeOutKnight.play());
        fadeOutKnight.setOnFinished(event -> fadeInMushroom.play());
        fadeInMushroom.setOnFinished(event -> fadeOutMushroom.play());
        fadeOutMushroom.setOnFinished(event -> fadeInGolem.play());

        fadeInGolem.play();
    }
}
