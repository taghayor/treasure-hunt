package client.view.multiscene.model;

import client.Client;

/**
 * Marks the ability of a controller to hold a client.
 *
 * @author Ellenor Taghayor
 * @version 2021-05-15
 */

public interface ClientHolder {

    /** @param client the client to set */

    void setClient(Client client);
}
