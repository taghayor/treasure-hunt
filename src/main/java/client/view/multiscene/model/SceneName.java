package client.view.multiscene.model;


public enum SceneName {
    CHOOSE_CHARACTER, LOGIN_MENU, LOBBY, CREATE_GAME_MENU, JOIN_GAME_MENU, IN_GAME,WAITING_ROOM;
}