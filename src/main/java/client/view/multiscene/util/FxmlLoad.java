package client.view.multiscene.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


import client.view.multiscene.model.ClientHolder;
import client.view.multiscene.model.Stageable;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;


/**
 * @author Knute Snortum
 * @version 2019-01-30
 */
public class FxmlLoad {

//	private static Logger logger = LogManager.getLogger();

    /**
     * Either builds the scene from {@link FxmlInfo} or loads the built scene.<br>
     * <br>
     * Uses this class's ClassLoader to find the URL of the FXML file.  If the URL
     * is {@code null} then the FXML file could not be found.
     *
     * @param fxmlInfo the FXML file info to load the scene with
     * @return the built scene, or {@code null} if there was an error
     */
    public Scene load(FxmlInfo fxmlInfo) {
        if (fxmlInfo.hasScene()) {
            System.out.println(fxmlInfo.getResourceName());
            return fxmlInfo.getScene();
        }

        URL url = null;
        try {
            url = new File(fxmlInfo.getResourceName()).toURI().toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (url == null) {
//			logger.error("The URL for the resource \"{}\" was not found", fxmlInfo.getResourceName());
//			debugInfo(fxmlInfo.getResourceName()); // not required
            Platform.exit();
            return null;
        }

        FXMLLoader loader = new FXMLLoader(url);
        Scene scene;

        try {
            scene = new Scene(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
            Platform.exit();
            return null;
        }

        // Write back the updated FxmlInfo to the scenes Map in ClientGUI


        Stageable controller = loader.getController();
        if (controller != null) {
            controller.setStage(fxmlInfo.getStage());
        }
        ClientHolder controller2 = loader.getController();
        if (controller2 != null) {
            controller2.setClient(fxmlInfo.getClient());

        }
        fxmlInfo.setScene(scene);
        fxmlInfo.getGui().updateScenes(fxmlInfo.getSceneName(), fxmlInfo);
        return scene;
    }

}
