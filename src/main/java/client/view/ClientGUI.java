package client.view;

import assets.Images;

import client.Client;
import client.view.multiscene.model.SceneName;
import client.view.multiscene.util.FxmlInfo;
import commun.Board;
import commun.GameMode;
import commun.Box;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The main class for view.
 * It connects all the fxml controllers and is the responsible of changing scenes.
 * All attributes that are used only in gui mode are stored in this class.
 */
public class ClientGUI extends Application implements Initializable {

    private static final String CHOOSE_CHARACTER_FXML = "src/main/java/client/view/multiscene/controller/choose-character.fxml";
    private static final String LOBBY_FXML = "src/main/java/client/view/multiscene/controller/lobby.fxml";
    private static final String CREATE_GAME_FXML = "src/main/java/client/view/multiscene/controller/create-game-menu.fxml";
    private static final String JOIN_GAME_FXML = "src/main/java/client/view/multiscene/controller/join-game-menu.fxml";
    private static final String WAITING_ROOM_FXML = "src/main/java/client/view/multiscene/controller/waiting-room.fxml";
    private static final String IN_GAME_FXML = "src/main/java/client/view/multiscene/controller/in-game.fxml";
    private static final String LOGIN_SCREEN_FXML = "src/main/java/client/view/multiscene/controller/login-screen.fxml";

    /**
     * Maps each SceneName to its corresponding fxml and controller.
     */
    private final Map<SceneName, FxmlInfo> scenes = new HashMap<>();

    private Client client;
    private Stage stage;
    private String pseudo;
    /**
     * Used in waitingRoom waiting threads, for each map creator and map joiner.
     */
    private boolean gameStarted, gameStartRequested;
    /**
     * Used in waitingRoom waiting threads, for each map creator and map joiner.
     */
    private boolean joinThreadRunning, createThreadRunning;

    private boolean canvasShouldDraw;
    private int nbOfHolesWarFog = 0;
    /**
     * Stores all the available maps in JoiningMenu.
     * Its value is updated when the server accepts the {@code getList()} request.
     */
    private ListView<String[]> gamesListView;

    private int startX, startY, boxSize;

    public ClientGUI() {
        super();
    }

    /* IN GAME MENU HANDLERS */

    /**
     * The first call for displaying the gameboard.
     * Resets all inGame data, changes the visibility of all of the components according to the gameMode
     * and starts the first display {@link #refreshBoard()}.
     */
    public void initGameView() {
        Platform.runLater(() -> {
            resetGUIdata();
            initInGameController();
            Scene inGame = scenes.get(SceneName.IN_GAME).getScene();
            scenes.get(SceneName.WAITING_ROOM).getStage().setScene(inGame);
            canvasShouldDraw = true;
            // etiendre turnLabel
            if (client.getGame().getGameMode() == GameMode.SPEEDING_CONTEST.getValue()) {
                Parent root = scenes.get(SceneName.IN_GAME).getScene().getRoot();
                Label turnLabel = (Label) root.getChildrenUnmodifiable().get(0);
                turnLabel.setVisible(false);
            } else if (client.getGame().getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue()) {
                Parent root = scenes.get(SceneName.IN_GAME).getScene().getRoot();
                Label hintsLabel = (Label) root.getChildrenUnmodifiable().get(4);
                Label buyMapPartButton = (Label) root.getChildrenUnmodifiable().get(3);
                Label buyHolesButton = (Label) root.getChildrenUnmodifiable().get(2);
                Label nbOfHolesLabel = (Label) root.getChildrenUnmodifiable().get(7);
                hintsLabel.setVisible(true);
                buyHolesButton.setVisible(true);
                buyMapPartButton.setVisible(true);
                nbOfHolesLabel.setVisible(true);
                showNbOfHolesWarFog(nbOfHolesWarFog);
            }
            refreshBoard();

        });
    }

    /**
     * Gets access to the drawing canvas and starts drawing using {@link ResizableCanvas#draw()} )}
     */
    public void refreshBoard() {
        Platform.runLater(() -> {
            Parent root = scenes.get(SceneName.IN_GAME).getScene().getRoot();
            Pane canvasContainer = (Pane) (root.getChildrenUnmodifiable().get(1));
            ResizableCanvas canvas = (ResizableCanvas) canvasContainer.getChildren().get(0);
            canvas.draw();
        });

    }

    /**
     * Draws the whole gameBoard.
     *
     * @param gc     the GraphicsContext of the {@link ResizableCanvas}
     * @param width  the width of the canvas
     * @param height the height of the canvas
     */
    private void displayBoard(GraphicsContext gc, int width, int height) {

        if (!canvasShouldDraw)
            return;
        Board board = client.getGame().getBoard();
        int targetWidth = width / (board.getWidth() + 2);
        int targetHeight = height / (board.getHeight() + 2);
        this.boxSize = Math.min(targetWidth, targetHeight); // chooses the minimum size for the boxes to be square
        // calculates the x and y of the upper left point for starting the drawing
        this.startX = (width - ((board.getWidth() + 2) * boxSize)) / 2;
        this.startY = (height - ((board.getHeight() + 2) * boxSize)) / 2;

        if (client.getGame().getGameMode() == GameMode.BROUILLARD_DE_GUERRE.getValue()) {
            this.displayBoardWarFog(gc, startX, startY, boxSize);
            return;
        }
        Image baricadeScaled = Images.getBaricadeImage(boxSize, true, false);
        Image floorScaled = Images.getFloorImage(boxSize, true, false);
        for (int i = 0; i < board.getWidth() + 2; i++) {
            for (int j = 0; j < board.getHeight() + 2; j++) {
                gc.drawImage(floorScaled, startX + i * boxSize, startY + j * boxSize);
                if (board.getBoxes()[j][i] == null)
                    gc.drawImage(baricadeScaled, startX + i * boxSize, startY + j * boxSize);
                else {
                    if (board.getElement(j, i) != null) {
                        gc.drawImage(board.getElement(j, i).getImage(boxSize, true, false), startX + i * boxSize,
                                startY + j * boxSize);
                    }
                }
            }
        }
    }

    /**
     * Increments the players score after receiving MOVE_OK_TREASURE answer from server.
     */
    public void incrementPlayerScoreDisplay() {
        Platform.runLater(() -> {
            Parent root = scenes.get(SceneName.IN_GAME).getScene().getRoot();
            Label scoreL = (Label) root.getChildrenUnmodifiable().get(5);
            scoreL.setText("Coins " + client.getPlayer().getPlayerScore());
        });

    }

    /**
     * Shows the corresponding dialogue to each player after their game is over and brings them back to the lobby.
     * Called after the server sends move_dead and game_end_broadcast to the client.
     */
    public void gameOver(boolean dead, boolean won, String name) {
        if (dead)
            showInformationDialogue("Game Over", "You are dead:(");
        else {
            if (won)
                showInformationDialogue("CONGRATS", "You won the game!");
            else
                showInformationDialogue("Game Over", "Player " + name + " won the game!");
        }
        scenes.get(SceneName.IN_GAME).getStage().setScene(scenes.get(SceneName.LOBBY).getScene());
    }

    /* WAR FOG AND ROUND BY ROUND HANDLERS */

    /**
     * Special drawing for the WarFog mode. called in {@link #displayBoard(GraphicsContext, int, int)}
     *
     * @param gc         the GraphicsContext of the {@link ResizableCanvas}
     * @param startX     the starting point's X for drawing calculated in {@link #displayBoard(GraphicsContext, int, int)}
     * @param startY     the starting point's Y for drawing calculated in {@link #displayBoard(GraphicsContext, int, int)}
     * @param targetSize the box size calculated in {@link #displayBoard(GraphicsContext, int, int)}
     */
    private void displayBoardWarFog(GraphicsContext gc, int startX, int startY, int targetSize) {
        Board board = client.getGame().getBoard();
        Image baricadeScaled = Images.getBaricadeImage(targetSize, true, false);
        Image floorScaled = Images.getFloorImage(targetSize, true, false);
        Image hiddenBox = Images.getHiddenBox(targetSize, true, false);

        Box[][] boxes = board.getBoxes();
        for (int i = 0; i < board.getWidth() + 2; i++) {
            for (int j = 0; j < board.getHeight() + 2; j++) {
                gc.drawImage(floorScaled, startX + i * targetSize, startY + j * targetSize);

                boolean isTreasure = false;
                if (boxes[j][i] != null && boxes[j][i].getElement() != null) {
                    isTreasure = boxes[j][i].getElement().isTreasure();
                }

                if (boxes[j][i] == null)
                    gc.drawImage(baricadeScaled, startX + i * targetSize, startY + j * targetSize);
                else if (client.getPlayer().isInShownPart(j, i) || isTreasure) {
                    if (board.getElement(j, i) != null) {
                        gc.drawImage(board.getElement(j, i).getImage(targetSize, true, false), startX + i * targetSize,
                                startY + j * targetSize);
                    }
                } else {
                    gc.drawImage(hiddenBox, startX + i * targetSize, startY + j * targetSize);
                }
            }
        }
    }

    /**
     * Updates the turn label for the player after each player's move.
     * Called after the server sends the inTurn player name to all the players.
     */
    public void showTurn(String turnPerson) {
        Platform.runLater(() -> {
            Parent root = scenes.get(SceneName.IN_GAME).getScene().getRoot();
            Label turnLabel = (Label) root.getChildrenUnmodifiable().get(0);

            String[] data = turnPerson.split("/");
            String pseudo = data[0];
            String heroName = data[1];

            if (turnPerson.equals(client.getPlayer().getFullName())) {
                turnLabel.setText("It's your turn, " + heroName + " " + pseudo + "!");
                turnLabel.setTextFill(Color.GREEN);
            } else {
                turnLabel.setText(
                        "Sorry " + client.getPlayer().getHeroName() + ", it's " + heroName + " " + pseudo + "'s turn");
                turnLabel.setTextFill(Color.RED);
            }

        });

    }

    /**
     * Calculates and updates the numberOfHolesAround label in WarFog mode.
     *
     * @param nb the number of holes in the 8 boxes around the player
     */
    public void showNbOfHolesWarFog(int nb) {
        Platform.runLater(() -> {
            Parent root = scenes.get(SceneName.IN_GAME).getScene().getRoot();
            Label nbOfHolesLabel = (Label) root.getChildrenUnmodifiable().get(7);
            if (nb == 1)
                nbOfHolesLabel.setText("There is " + nb + " hole around you");
            else
                nbOfHolesLabel.setText("There are " + nb + " holes around you");
            this.nbOfHolesWarFog = nb;
            if (nb > 0)
                nbOfHolesLabel.setTextFill(Color.RED);
            else
                nbOfHolesLabel.setTextFill(Color.GREEN);
        });
    }

    /* CREATE GAME MENU OBJECTS HANDLERS */

    /**
     * Sends the map creator to the waiting room and starts the waiting thread. The creator waits in the waiting room
     * until at least one persons joins the game;
     * after that the startGame button becomes visible for the creator to send the startGame request to the server.
     */
    public void confirmMapCreated() {

        Platform.runLater(() -> {
            scenes.put(SceneName.WAITING_ROOM, new FxmlInfo(WAITING_ROOM_FXML, SceneName.WAITING_ROOM, stage, client));
            Parent waitRoot = scenes.get(SceneName.WAITING_ROOM).getScene().getRoot();
            scenes.get(SceneName.CREATE_GAME_MENU).getStage().setScene(scenes.get(SceneName.WAITING_ROOM).getScene());
            createThreadRunning = true;
            Thread t = new Thread(() -> {
                boolean alreadyClicked = false;
                while (!gameStarted && createThreadRunning) {
                    int nbOfPlayers = client.getGame().getPlayers().size() + 1;
                    if (nbOfPlayers >= 2 && !alreadyClicked) {
                        Label startLabel = (Label) waitRoot.getChildrenUnmodifiable().get(1);
                        startLabel.setVisible(true);
                        alreadyClicked = true;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {

                        e.printStackTrace();
                    }
                }

            });
            t.start();
        });
    }

    /* JOIN GAME MENU HANDLERS */

    /**
     * Sends the map joiner to the waiting room and starts the waiting thread.
     * The joiner waits for the creator to send the starting request.
     * after that the accept and refuse buttons become visible for the joiner to send their answer to the server.
     * If the joiner accepts the start, he has to wait for all
     * other players to accept, else he's brought back to the lobby.
     */
    public void joinResponse() {

        Platform.runLater(() -> {
            scenes.put(SceneName.WAITING_ROOM, new FxmlInfo(WAITING_ROOM_FXML, SceneName.WAITING_ROOM, stage, client));
            Parent waitRoot = scenes.get(SceneName.WAITING_ROOM).getScene().getRoot();
            scenes.get(SceneName.JOIN_GAME_MENU).getStage().setScene(scenes.get(SceneName.WAITING_ROOM).getScene());
            joinThreadRunning = true;
            Thread t = new Thread(() -> {
                while (!gameStarted && joinThreadRunning) {
                    // int nbOfPlayers = client.getGame().getPlayers().size() + 1;
                    // System.out.println(nbOfPlayers);
                    if (gameStartRequested) {
                        Label label = (Label) waitRoot.getChildrenUnmodifiable().get(0);
                        Label acceptStart = (Label) waitRoot.getChildrenUnmodifiable().get(2);
                        acceptStart.setVisible(true);
                        Label refuseStart = (Label) waitRoot.getChildrenUnmodifiable().get(3);
                        refuseStart.setVisible(true);
                        gameStartRequested = false;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            t.start();
        });
    }

    /**
     * Called after the server sends the list of available games to the client after the {@code getList()} request.
     * updates and adds listeners for each playable game.
     */
    public void showListOfGames() {

        Platform.runLater(() -> {
            ObservableList<String[]> gamesIds = FXCollections.observableArrayList(client.getGamesStrings());
            gamesListView = (ListView<String[]>) scenes.get(SceneName.JOIN_GAME_MENU).getScene().getRoot()
                    .getChildrenUnmodifiable().get(0);
            gamesListView.setCellFactory(param -> new ListCell<>() {

                @Override
                protected void updateItem(String[] item, boolean empty) {
                    AtomicBoolean mouseClicked = new AtomicBoolean(false);
                    super.updateItem(item, empty);
                    setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
                    setTextFill(Color.WHITE);
                    setFont(new Font(22));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextAlignment(TextAlignment.LEFT);
                    setOnMouseEntered(event -> {
                        if (!mouseClicked.get())
                            setTextFill(Color.LIGHTGREEN);
                    });
                    setOnMouseExited(event -> {
                        if (!mouseClicked.get())
                            setTextFill(Color.WHITE);
                    });
                    setOnMouseClicked(event -> {
                        setTextFill(Color.CHARTREUSE);
                        mouseClicked.set(true);
                    });
                    if (empty || item == null) {
                        setText(null);
                    } else {
                        String mode = String.valueOf(GameMode.values()[Integer.parseInt(item[5]) - 1]);
                        String size = item[6] + " , " + item[7];
                        String holes = item[8];
                        String tres = item[9];
                        String hfill = "    ";
                        setText(mode + hfill + size + hfill + hfill + hfill + holes + hfill + hfill + hfill + hfill
                                + hfill + tres);
                    }
                }
            });
            gamesListView.setItems(gamesIds);
            scenes.get(SceneName.LOBBY).getStage().setScene(scenes.get(SceneName.JOIN_GAME_MENU).getScene());

        });
    }

    /**
     * Resets all the inGame data for every new game that has been started.
     */
    public void resetGUIdata() {
        gameStarted = false;
        gameStartRequested = false;
        joinThreadRunning = false;
        createThreadRunning = false;
        canvasShouldDraw = false;
    }

    /* WAITING ROOM HANDLERS */

    /**
     * Shows the list of the names of all the players who have refused the starting game to the creator.
     * Then the poor lonely creator is sent back to the lobby:(<3
     */
    public void gameAborted(StringBuilder data) {
        Platform.runLater(() -> {
            showInformationDialogue("Game Aborted", data.toString());
            scenes.get(SceneName.WAITING_ROOM).getStage().setScene(scenes.get(SceneName.LOBBY).getScene());
            resetGUIdata();
        });
    }
    /* LOBBY OBJECTS HANDLERS */

    /**
     * Sends the player with their chosen pseudo and character to the lobby
     * after the server has accepted their login.
     */
    public void loginAccepted() {
        Platform.runLater(() -> {
            scenes.get(SceneName.CHOOSE_CHARACTER).getStage().setScene(scenes.get(SceneName.LOBBY).getScene());
        });
    }


    /**
     * the main JAVAFX method to start the view.
     * Instances the client and sets the main stage and scenes.
     * Puts all the read fxmls to the {@code scenes} hashMap.
     */
    @FXML
    @Override
    public void start(Stage stage) {
        Platform.runLater(() -> {
            try {
                this.client = new Client(5000, this, true);
                this.client.initThreadForGUI();

            } catch (IOException e) {
                e.printStackTrace();
            }

            scenes.put(SceneName.CHOOSE_CHARACTER,
                    new FxmlInfo(CHOOSE_CHARACTER_FXML, SceneName.CHOOSE_CHARACTER, stage, this.client));
            scenes.put(SceneName.LOBBY, new FxmlInfo(LOBBY_FXML, SceneName.LOBBY, stage, this.client));
            scenes.put(SceneName.CREATE_GAME_MENU,
                    new FxmlInfo(CREATE_GAME_FXML, SceneName.CREATE_GAME_MENU, stage, client));
            scenes.put(SceneName.JOIN_GAME_MENU, new FxmlInfo(JOIN_GAME_FXML, SceneName.JOIN_GAME_MENU, stage, client));
            scenes.put(SceneName.LOGIN_MENU, new FxmlInfo(LOGIN_SCREEN_FXML, SceneName.LOGIN_MENU, stage, client));
            scenes.put(SceneName.WAITING_ROOM, new FxmlInfo(WAITING_ROOM_FXML, SceneName.WAITING_ROOM, stage, client));
            initInGameController();

            stage.setScene(scenes.get(SceneName.LOGIN_MENU).getScene());
            stage.setTitle("TREASURE HUNT");
            stage.getIcons().add(Images.getIconImage());
            // stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
            this.stage = stage;
        });

    }

    public void startGUI() {
        launch();
    }

    /* UTIL */


    /**
     * Resets the inGame components for it to be ready for each new game.
     */
    private void initInGameController() {
        scenes.put(SceneName.IN_GAME, new FxmlInfo(IN_GAME_FXML, SceneName.IN_GAME, stage, client));
        Parent root = scenes.get(SceneName.IN_GAME).getScene().getRoot();
        ResizableCanvas canvas = new ResizableCanvas();
        Pane canvasContainer = (Pane) (root.getChildrenUnmodifiable().get(1));
        canvasContainer.getChildren().clear();
        canvasContainer.getChildren().add(canvas);
        canvas.widthProperty().bind(canvasContainer.widthProperty());
        canvas.heightProperty().bind(canvasContainer.heightProperty());

    }

    /**
     * Auxiliary method for showing default Information alert dialogue.
     */
    public void showInformationDialogue(String title, String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setContentText(text);
//        alert.getGraphic().setStyle("-fx-background-color: black");
        alert.showAndWait();
    }

    /**
     * Auxiliary method for showing default Option alert dialogue.
     */
    public Optional<ButtonType> showOptionalDialogue(String title, String text) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(text);
//        alert.getGraphic().setStyle("-fx-background-color: black");
        return alert.showAndWait();
    }

    public Map<SceneName, FxmlInfo> getScenes() {
        return scenes;
    }

    public void updateScenes(SceneName name, FxmlInfo info) {
        scenes.put(name, info);
    }

    public Client getClient() {
        return client;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


    public void setGameStarted(boolean gameStarted) {
        this.gameStarted = gameStarted;
    }

    public void setGameStartRequested(boolean gameStartRequested) {
        this.gameStartRequested = gameStartRequested;
    }

    @Override
    public void stop() throws Exception {
        // TODO Auto-generated method stub
        super.stop();
        // we can broadcast the death here

        System.out.println("END OF APPLICATION");
        System.exit(0);
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public int getBoxSize() {
        return boxSize;
    }

    public int getStartY() {
        return startY;
    }

    public int getStartX() {
        return startX;
    }

    /**
     * Custom Canvas class for redrawing the board after each stage resize.
     */
    public class ResizableCanvas extends Canvas {

        public ResizableCanvas() {
            // Redraw canvas when size changes.
            widthProperty().addListener(evt -> draw());
            heightProperty().addListener(evt -> draw());
        }

        /**
         * Refreshes the gameBoard.
         */
        public void draw() {
            double width = getWidth();
            double height = getHeight();

            GraphicsContext gc = getGraphicsContext2D();
            gc.clearRect(0, 0, width, height);
            displayBoard(gc, (int) width, (int) height);

        }

        /**
         * Refreshes the gameBoard and draws the WarFog RevealMap Help zone.
         */
        public void draw(int x, int y, int w, int h) {
            double width = getWidth();
            double height = getHeight();

            GraphicsContext gc = getGraphicsContext2D();
            gc.clearRect(0, 0, width, height);
            displayBoard(gc, (int) width, (int) height);
            Color c = Color.CHARTREUSE;
            Color opaqueC = new Color(c.getRed(), c.getGreen(), c.getBlue(), 0.2);
            gc.setFill(opaqueC);
            gc.setStroke(c);
            gc.fillRect(x, y, w, h);
//
        }


        @Override
        public boolean isResizable() {
            return true;
        }

        @Override
        public double prefWidth(double height) {
            return getWidth();
        }

        @Override
        public double prefHeight(double width) {
            return getHeight();
        }
    }

}
